# Comment ajouter/editer un exercice ?

## La structure du dépôt Gitlab

* Le répertoire `site` contient les différents exercices.
* Le document `_data/navigation.yml` contient la description du sommaire (à gauche) du site.
* Le document `_config.yml` contient les paramêtres du site utilisés pour la génération.
* Le répertoire `_layouts` contient les patrons des pages HTML générées.
* Le répertoire `_includes` contient les patrons des fragments HTML utilisés pour la génération des pages HTML.
* Les pages HTML sont générées dans le répertoire `_site`
* Le répertoire `assets` contient le `main.scss` quit fait références à tous les autres scss du site. Il contient également le fichier de configuration de la recherche.
* Le répertoire `_sass` contient tous les `.scss` du site.
* Dans le dossier `site`, on retrouve l'ensemble des tutoriels oragnisés dans différents dossiers (`_duino_tuto`, `_riot_tuto`, etc.) et un dossier `assets` qui contient l'ensemble des images du sites. Le dossier `cover` contient les vignettes des pages, et le dossier `images` contients les images à l'interieur du site.


## Ajouts d'exercices

Metez dans le répertoire correspondant a votre exercice dans le reportoire `fr` les documents Markdown de votre exercice.

Mettez dans le dossier correspondant a votre exercices dans répertoire `fr/assets/images` les images de votre exercice.

Ajoutez une cover à votre tutoriel dans le repertoire `fr/assets/cover` (ajoutez en une nouvelle ou utilisez une déjà présente)

Mettre a jour l'odre de la collection dans le fichier `_config.yml` avec le nom du fichier Markdown ajouté.

## Modifications d'exercices

Modifiez le fichiers Markdown de l'exercice dans `fr`

Si le nom du fichier Markdown est modifié, mettre a jour l'odre de la collection dans le fichier `_config.yml`

## Ajout d'une catégorie

Pour ajouter une catégorie sur le site, ajoutez l'index de celle-ci (fichier Markdown) dans `fr\_pages`

Creez un nouveau repertoire dans le dossier `fr`, en n'oubliant le pas le `_` devant. Ajoutez-y vos différents tutoriels

Mettez a jour la collection dans le fichier `_config.yml`. Pour cela, ajoutez une collection avec le nom de votre repertoire créer précedemment mais cette fois sans le `_`.

Et enfin, mettez à jour le document `_data/navigation.yml` avec votre nouvelle catégorie.

## Edition depuis votre poste de travail

L'édition des documents peut se faire directement depuis le site https://gitlab.com/stm32python/fr-version-lora . Cependant, chaque modification déclenche le pipeline CI de regénération des pages. Cette opération est lente et consomme de l'énergie.

Vous pouvez éditer depuis votre poste de travail plusieurs pages du site et vérifier le rendu depuis votre poste de travail.

Pour cela, vous devez installer le générateur Jekyll de la facon suivante:
```bash
git clone git@gitlab.com:stm32python/fr-version-lora.git
cd fr-version-lora
bundle update
bundle install
bundle exec jekyll build
bundle exec jekyll serve
```

Ouvrez la page `http://127.0.0.1:4000/fr-version-lora/`


Récuperez les dernières modifications avec de commencer des éditions:
```bash
cd fr-version-lora
git pull
```

Editez les pages; le générateur Jekyll sera relancé à chaque sauvegarde. Fraichissez la page web dans votre navigateur pour constater les modifications effectuées.

Une fois que les ajouts et les modifications sont satisfaisantes, poussez les modifications vers le serveur central.
```bash
git add *
git commit -m "amélioration de ceci cela"
git push
```

Une fois le `push` effectué, vérifiez l'état du pipeline CI/CD https://gitlab.com/stm32python/fr-version-lora/pipelines

Visualisez les pages du site en ligne https://stm32python.gitlab.io/fr-version-lora/
