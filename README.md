# Pages de documentation de STM32Python avec la connectivité LoRa et LoRaWAN

Ce dépôt est dédié à la réalisation de tutoriel IoT pour les cartes munies des MCU [STM32WL55]() intégrant un transceiver LoRa SX126x.

Les cartes cibles sont :
* [NUCLEO-WL55JC](https://www.st.com/en/evaluation-tools/nucleo-wl55jc.html)
* [Seeedstudio LoRa E5](https://www.seeedstudio.com/LoRa-E5-Dev-Kit-p-4868.html)
* [Seeedstudio LoRa E5 Mini](https://www.seeedstudio.com/LoRa-E5-mini-STM32WLE5JC-p-4869.html)

## Getting Started
* [Install Jekyll](https://jekyllrb.com/docs/installation/)

### Clone the repo
```bash
mkdir ~/gitlab/stm32python
git clone git@gitlab.com:stm32python/fr-version-lora.git
cd fr
```

### Start locally
```bash
cd ~/gitlab/stm32python/fr-version-lora
bundle update
bundle install
bundle exec jekyll build
bundle exec jekyll serve
```

Browse http://127.0.0.1:4000/fr-version-lora/

### Start locally with Docker

```bash
cd ~/gitlab/stm32python/fr-version-lora
```

Build incrementally the site and serve it
```bash
docker run --rm \
  -p 4000:4000 \
  --volume="$PWD:/srv/jekyll" \
  --volume="$PWD/vendor/bundle:/usr/local/bundle" \
  -it jekyll/jekyll:3.8 \
  jekyll serve  --incremental
```

Browse http://0.0.0.0:4000/fr-version-lora/

### Deploy

Push your repository and changes to GitLab. Then check the CI pipeline https://gitlab.com/stm32python/fr/-/pipelines

## References
* [Contributeurs](CONTRIBUTORS.md)
* [TODO List](TODO.md)
* [CHANGELOG](CHANGELOG.md)
* [LICENSE](LICENSE.md)

## Troubleshooting

[ci]: https://about.gitlab.com/gitlab-ci/
[Jekyll]: http://jekyllrb.com/
[install]: https://jekyllrb.com/docs/installation/
[documentation]: https://jekyllrb.com/docs/home/
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
[jekyll-docker]: https://github.com/envygeeks/jekyll-docker/blob/master/README.md
