---
layout: article
title: Exercices simples avec RIOT OS pour la carte NUCLEO-WL55JC
description: Exercices simples avec RIOT OS pour la carte NUCLEO-WL55JC
permalink: nucleo-wl55jc.html
key: page-aside
cover: site/assets/images/lora/nucleo-wl55jc.png
aside:
  toc: true
sidebar:
  nav: site_nav
---

La MCU STM32WL55 de la carte NUCLEO-WL55JC inclut un transceiver LoRa qui permet des communications longue distance en LoRa et dans un réseau LoRaWAN comme les réseaux publics [TTN](https://eu1.cloud.thethings.network/console/applications) et [Helium](https://console.helium.com/devices) ou bien les réseaux privés opérés avec Chirpstack comme [CampusIoT](https://lns.campusiot.imag.fr).

La carte NUCLEO-WL55JC comporte un bouton et une DEL ainsi qu'un connecteur Arduino permettant d'ajouter des cartes filles comme la [carte d'extension de connection Grove](https://www.seeedstudio.com/Base-Shield-V2.html) qui servira à brancher des modules Grove ou la carte MEMS IKS01A3.

> *Attention: il existe une autre version NUCLEO-WL55JC2 de la carte qui permet de communiquer sur les bandes ISM 433 MHz (Europe) et 470 MHz (Chine).*

## Clignotement des trois DEL

Construisez le firmware de test.

```bash
cd RIOT/tests/leds/
make BOARD=nucleo-wl55jc
```

Chargez le firmware sur votre carte (ici `nucleo-wl55jc`).
```bash
make BOARD=nucleo-wl55jc flash-only
```

> Remarque: `make` execute OpenOCD pour le chargement.

> Il se peut que le chargement du firmware avec OpenOCD échoue si la version que vous utilisez est ancienne. Utilisez la version 0.11.0 d'OpenOCD.

Vous pouvez vous connecter au port console de la carte.
```
main(): This is RIOT! (Version: 2022.01-devel-217-g43bef)                    
On-board LED test                       
                                        
Available LEDs: 3                       
                                        
Will now light up each LED once short and twice long in a loop
```

Les 3 DELs clignotent les unes à la suite des autres.

## Utilisation des trois boutons utilisateur

Construisez le firmware de test.

```bash
cd RIOT/tests/buttons/
make BOARD=nucleo-wl55jc flash
```

Vous pouvez vous connecter au port console de la carte et appuyer sur les trois boutons de la carte.
```
Help: Press s to start test, r to print it is ready
START
main(): This is RIOT! (Version: 2022.01-devel-217-g43bef)
On-board button test

 -- Available buttons: 3

 -- Try pressing buttons to test.

[SUCCESS]
Pressed BTN0
Pressed BTN1
Pressed BTN2
Pressed BTN2
Pressed BTN0
Pressed BTN1
```

## Lecture de la température avec une sonde externe DS18B20

Connectez une [sonde externe de température DS1820 Grove](https://wiki.seeedstudio.com/One-Wire-température-Sensor-DS18B20/) à la broche la carte Nucleo WL55JC via la [carte d'extension de connection Grove](https://www.seeedstudio.com/Base-Shield-V2.html) via le port `D2`.

```bash
cd RIOT/tests/driver_ds18/
make BOARD=nucleo-wl55jc flash
```

Vous pouvez vous connecter au port console de la carte.
```
TODO
```

> La sonde thermomêtre [DS18B20](https://datasheets.maximintegrated.com/en/ds/DS18B20.pdf) est très répandue. Cette sonde existe avec ou sans la résistance intégrée de 4.7K oHm requis par le [bus 1-Wire](https://www.carnetdumaker.net/articles/faire-un-scanneur-de-bus-1-wire-avec-une-carte-arduino/).

> Elle peut être utilisé pour des mesures déportées de la carte (eau, glace ...).

## Carte d'extension IKS01A3

La [carte d’extension IKS01A3](https://www.st.com/en/ecosystems/x-nucleo-iks01a3.html) fait la démonstration de plusieurs capteurs MEMS (systèmes micro électro-mécaniques) de STMicroelectronics. Sa version A3 rassemble les capteurs suivants :

* [LSM6DSO](https://www.st.com/content/st_com/en/products/mems-and-sensors/inemo-inertial-modules/lsm6dso.html#tools-software) : Accéléromètre 3D + Gyroscope 3D
* [LIS2MDL](https://www.st.com/content/st_com/en/products/mems-and-sensors/e-compasses/lis2mdl.html) : Magnétomètre 3D
* [LIS2DW12](https://www.st.com/content/st_com/en/products/mems-and-sensors/accelerometers/lis2dw12.html) : Accéléromètre 3D
* [LPS22HH](https://www.st.com/content/st_com/en/products/mems-and-sensors/pressure-sensors/lps22hh.html) : Baromètre (260 à 1260 hPa)
* [HTS221](https://www.st.com/content/st_com/en/products/mems-and-sensors/humidity-sensors/hts221.html) : Capteur d’humidité relative
* [STTS751](https://www.st.com/content/st_com/en/products/mems-and-sensors/temperature-sensors/stts751.html) : Capteur de température (–40 °C à +125 °C)

![Carte d'extension IKS01A3](site/assets/images/microPython/iks01a3.png)


Une fois la carte d'extension placée sur les connecteurs Arduino, ces capteurs sont raccordés au bus *I2C* de la carte LoRa E5.

Les [pilotes de plusieurs de ces capteurs](https://github.com/RIOT-OS/RIOT/tree/master/drivers) sont implémentés par RIOT.

### Utilisation de Accéléromètre 3D + Gyroscope 3D LSM6DSO

```bash
cd RIOT/tests/driver_lsm6dsl/
make BOARD=nucleo-wl55jc flash
```

### Utilisation du Magnétomètre 3D LIS2MDL

```bash
cd RIOT/tests/driver_lis2dh12/
make BOARD=nucleo-wl55jc flash
```

### Utilisation du Baromètre LPS22HH

```bash
cd RIOT/tests/driver_lpsxxx/
make BOARD=nucleo-wl55jc flash
```

### Utilisation du Capteur de température HTS221

```bash
cd RIOT/tests/driver_hts221/
make BOARD=nucleo-wl55jc flash
```


```
H: 62.6%, T: 23.7?°C                                                            
H: 62.7%, T: 23.7?°C                                                            
H: 62.7%, T: 23.7?°C                                                            
H: 62.6%, T: 23.6?°C                                                            
```

## Lecture et écriture sur une carte MicroSD

Connectez une [carte d'extension comportant un lecteur MicroSD](https://learn.sparkfun.com/tutorials/microsd-shield-and-sd-breakout-hookup-guide/all) sur le brochage Arduino.

Insérez une carte MicroSD dans le lecteur MicroSD.

```
cd RIOT/tests/pkg_fatfs/
make BOARD=nucleo-wl55jc flash
```

```
cd RIOT/tests/pkg_fatfs_vfs/
make BOARD=nucleo-wl55jc flash
```

Vous pouvez vous connecter au port console de la carte.
```
TODO
```

> A noter: ce programme est l'embryon d'un "data logger" permettant de journaliser les mesures relevées sur des capteurs (température, humidité, pression atmosphérique, choc, latitude, longitude, altitude, ppm de C02, ppm de PM1/PM2.5/PM10, VOC, eCO2, distance, mouvement ...).


## Autres modules Grove intéressants

* [Grove - Formaldehyde Sensor](https://wiki.seeedstudio.com/Grove-Formaldehyde-Sensor/) contient un composant Sensirion SFA3X  
* [Grove - Doppler Radar](https://wiki.seeedstudio.com/Grove-Doppler-Radar/)
* [Grove - GPS (Air530)](https://wiki.seeedstudio.com/Grove-GPS-Air530/) dans [GPS Grove](https://wiki.seeedstudio.com/GPS-Modules-Selection-Guide/)
* [Grove - Human Presence Sensor (AK9753)](https://wiki.seeedstudio.com/Grove-Human_Presence_Sensor-AK9753/)
* [Grove - Q Touch Sensor](https://wiki.seeedstudio.com/Grove-Q_Touch_Sensor/)
* Modules chinois avec un [composant Aosong ASAIR AHT1x/AHT2x](https://github.com/enjoyneering/AHTxx)

## Galerie

![Nucleo WL55JC](site/assets/images/lora/nucleo-wl55jc.png)

![Grove shield](site/assets/images/lora/grove-shield.png)
