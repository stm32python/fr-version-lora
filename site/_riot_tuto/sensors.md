
- ---
layout: article
title: Exercices simples avec des capteurs 
description: Exercices simples avec des capteurs
permalink: riot_sensors.html
key: page-aside
cover: site/assets/images/stm32duino/capteur_temp.png
aside:
  toc: true
sidebar:
  nav: site_nav
---

## Affichage d'un message sur un afficheur LCD

Connectez le module [Grove LCD 16X2](https://wiki.seeedstudio.com/Grove-16x2_LCD_Series/) à un des deux ports Grove I2C de la carte

```
TODO
```

## Lecture de la température, de l'humidité et de la pression atmosphérique.

### Utilisation du capteur NXP LM75

La carte dispose d'un capteur NXP LM75 qui est connecté au bus I2C de la MCU

```bash
cd RIOT/tests/driver_lm75/
make BOARD=lora-e5-dev DRIVER=lm75a
```

Vous pouvez vous connecter au port console de la carte.
```
main(): This is RIOT! (Version: 2022.01-devel-429-g70744)                       
Sensor test...                                                                  
Initialization...                                                               
Initialization succeeded                                                        
error setting Hyst and/or OS temps                                              
Set OS temp is 80.0ºC                                                          
Set HYST temp is 75.0ºC                                                        
20.0ºC                                                                         
Set OS temp is 80.0ºC                                                          
Set HYST temp is 75.0ºC                                                        
lm75: OS alert pin not connected or defined                                     
Error reading OS pin state                                                      
22.500ºC                                                                       
```

### Utilisation du capteur BME680

Connectez le module [Grove BME680](https://wiki.seeedstudio.com/Grove-Temperature_Humidity_Pressure_Gas_Sensor_BME680/) à un des deux ports Grove I2C de la carte

```bash
cd RIOT/tests/driver_bme680/
make BOARD=lora-e5-dev
```

Vous pouvez vous connecter au port console de la carte.
```
TODO
```

> A noter: ce programme est l'embryon d'une station météo

### Utilisation d'une sonde externe DS18B20

Connectez une [sonde externe de température DS1820 Grove](https://wiki.seeedstudio.com/One-Wire-température-Sensor-DS18B20/) à la broche `D2` de la carte LoRa E5.

```bash
cd RIOT/tests/driver_ds18/
make BOARD=lora-e5-dev
```

Vous pouvez vous connecter au port console de la carte.
```
TODO
```

> La sonde thermomêtre [DS18B20](https://datasheets.maximintegrated.com/en/ds/DS18B20.pdf) est très répandue. Cette sonde existe avec ou sans la résistance intégrée de 4.7K oHm requis par le [bus 1-Wire](https://www.carnetdumaker.net/articles/faire-un-scanneur-de-bus-1-wire-avec-une-carte-arduino/).

> Elle peut être utilisé pour des mesures déportées de la carte (eau, glace ...).


## Lecture de la qualité de l'air

### Utilisation du capteur SGP30

Connectez le module [Grove _Qualité de l'Air_ VOC et eCO2 (SGP30)](https://wiki.seeedstudio.com/Grove-VOC_and_eCO2_Gas_Sensor-SGP30/) à un des deux ports Grove I2C de la carte

```bash
cd RIOT/tests/driver_sgp30/
make BOARD=lora-e5-dev
```

Vous pouvez vous connecter au port console de la carte.
```
TODO
```

> A noter: ce programme est l'embryon d'une station de mesure de la qualité de l'air.

### Utilisation du capteur CCS811

Connectez le module _Qualité de l'Air_ [Sparkfun CCS811](https://www.sparkfun.com/products/14193) ou [Adafruit CCS811](https://www.adafruit.com/product/3566) à un des deux ports Grove I2C de la carte ou les broches Arduino I2C.

> Ces modules mesurent la contentration en _eCO2_ (_equivalent calculated carbon-dioxide_) dans un interval de 400 à 8192 _parts per million_ (ppm), et la concentration en _TVOC_ (_Total Volatile Organic Compound_) dans un interval de 0 à 1187 _parts per billion_ (ppb). D'après leurs _datasheets_, ils peuvent détecter les alcools, les aldehydes, les cétones, les acides organiques, les amines et les hydrocarbures aromatiques et aliphatiques.

```bash
cd RIOT/tests/driver_ccs811_full/
make BOARD=lora-e5-dev
```

Vous pouvez vous connecter au port console de la carte.
```
TODO
```

> A noter: ce programme est l'embryon d'une station de mesure de la qualité de l'air.

### Utilisation du capteur PPD42NS de particules fines

Connectez le module [Grove _Particules fines_ (Dust Sensor PPD42NS)](https://wiki.seeedstudio.com/Grove-Dust_Sensor/) à une des broches GPIO de la carte

<br>
<div align="center">
<img alt="dust" src="site/assets/images/lora/dust2.jpeg" heigth="600px" width="600px">
</div> <br>



- connectez  le capteur de dust à la carte Lora en suivant le cablage dans l’image si dessus 
(il est important de connecter le capteur à  la pin D10  de la carte lora )

- connectez la carte STM32 à la carte lora-e5 mini pour que vous puissiez flasher la carte lora 

- mettez en oeuvre l’antenne à la carte lora pour pourvoir envoyer et recevoir des données 

- compilez le code avec la commande suivante 
```bash
cd RIOT/tests/lora_dust/
make BOARD=lora-e5-dev LORA_DRIVER=sx126x_stm32wl
```
- flashez la carte lora avec le programme STM32 cube en vous appuyant sur les  [instructions données par seeedstudio](https://wiki.seeedstudio.com/LoRa_E5_Dev_Board/#23-build-the-lorawan-end-node-example)

- ouvrez une console sur votre carte en utilisant arduino ou telnet
<br>
<div align="center">
<img alt="dust" src="site/assets/images/lora/dust_lecture.png" heigth="600px" width="600px">
</div> <br>


> A noter: ce programme est l'embryon d'une station de mesure de la qualité de l'air.

### Utilisation du capteur SCD30 Co2 & Temperature & Humidity

Connectez le module [Grove - CO2 & Temperature & Humidity Sensor (SCD30)](https://wiki.seeedstudio.com/Grove-CO2_Temperature_Humidity_Sensor-SCD30/) à une des broches GPIO de la carte

<br>
<div align="center">
<img alt="dust" src="site/assets/images/lora/co2_cablage.jpeg" heigth="600px" width="600px">
</div> <br>



- connectez  le capteur de co2 à la carte Lora en suivant le cablage dans l’image si dessus 
(il est important de connecter le capteur à  la pin D10  de la carte lora )

- connectez la carte STM32 à la carte lora-e5 mini pour que vous puissiez flasher la carte lora 

- mettez en oeuvre l’antenne à la carte lora pour pourvoir envoyer et recevoir des données 

- compilez le code avec la commande suivante 
```bash
cd RIOT/tests/lora_co2_scd30/
make BOARD=lora-e5-dev LORA_DRIVER=sx126x_stm32wl
```
- flashez la carte lora avec le programme STM32 cube en vous appuyant sur les  [instructions données par seeedstudio](https://wiki.seeedstudio.com/LoRa_E5_Dev_Board/#23-build-the-lorawan-end-node-example)

- ouvrez une console sur votre carte en utilisant arduino ou telnet
<br>
<div align="center">
<img alt="dust" src="site/assets/images/lora/co2_lecture.png" heigth="600px" width="600px">
</div> <br>


> A noter: ce programme est l'embryon d'une station de mesure de la qualité de l'air.

## Lecture de l'accélération

### Utilisation du Grove Accélérometre ADXL354

Connectez le module [Grove Accélérometre ADXL354](https://wiki.seeedstudio.com/Grove-3-Axis_Digital_Accelerometer-16g/) à un des deux ports Grove I2C de la carte

```bash
cd RIOT/tests/driver_adxl345/
make BOARD=lora-e5-dev
```

Vous pouvez vous connecter au port console de la carte.
```
TODO
```

> A noter: ce programme est l'embryon d'un capteur de choc, d'un antivol (véhicule, tableau de maître ...).

### Utilisation du Grove Accélérometre et Gyrosccope LSM6DS3

Connectez le module [Grove - 6-Axis Accelerometer&Gyroscope](https://wiki.seeedstudio.com/Grove-6-Axis_AccelerometerAndGyroscope/) à un des deux ports Grove I2C de la carte. Ce module inclut un Accelerometer&Gyroscope is LSM6DS3

```bash
cd RIOT/tests/driver_lsm6dsl/
make BOARD=lora-e5-dev
```

Vous pouvez vous connecter au port console de la carte.
```
TODO
```

> A noter: ce programme est l'embryon d'un capteur de choc, d'un antivol (véhicule, tableau de maître ...).


## Emission d'une alerte sonore

Connectez le module [Grove _Buzzer piezo_](https://wiki.seeedstudio.com/Grove-Buzzer/) à une broche GPIO de la carte

```
cd RIOT/tests/periph_gpio/
make BOARD=lora-e5-dev
```

Vous pouvez vous connecter au port console de la carte.
```
TODO
```

> A noter: ce programme est l'embryon de la sirène d'alarme d'une station de mesure de la qualité de l'air, d'une centrale d'alarme, antivol véhicule ...

## Détection de mouvement par infrarouge

Connectez le module [Grove _infrarouge PIR_](https://wiki.seeedstudio.com/Grove-PIR_Motion_Sensor/) à une broche GPIO de la carte

```
TODO
```

Vous pouvez vous connecter au port console de la carte.
```
TODO
```

> A noter: ce programme est l'embryon d'un détecteur de présence dans une pièce.

## Mesure d'une distance

Connectez le module [Grove _Radar Ultrason_](https://wiki.seeedstudio.com/Grove-Ultrasonic_Ranger/) à deux broches GPIO de la carte

```
TODO
```

Vous pouvez vous connecter au port console de la carte.
```
TODO
```

> A noter: ce programme est l'embryon d'un détecteur de place de parking libre dans un garage couvert, un capteur de niveau de remplissage d'une cuve (eau, fuel ...) ou d'une poubelle.

## Mesure de la qualité de l'eau

Connectez un ou plusieurs de ces capteurs aux broches  analogiques (`A1`, `A2`, `A3`, `A4`) de la carte LoRa E5
* [Grove - ORP Sensor Kit (501Z)](https://wiki.seeedstudio.com/Grove-ORP-Sensor-kit/)
* [Grove - EC Sensor Kit](https://wiki.seeedstudio.com/Grove-EC-Sensor-kit/)
* [Grove - PH Sensor Kit (E-201C-Blue)](https://wiki.seeedstudio.com/Grove-PH-Sensor-kit/)
* [Grove - Turbidity Sensor Meter for Arduino V1.0](https://wiki.seeedstudio.com/Grove-Turbidity-Sensor-Meter-for-Arduino-V1.0/)

Connectez également une sonde externe DS1820 car la mesure analogique a besoin d'être corrigée avec la valeur de température du milieu.

```
cd RIOT/tests/periph_gpio/
make BOARD=lora-e5-dev
```

Vous pouvez vous connecter au port console de la carte.
```
TODO
```

## Lecture d'une horloge temps réel

Connectez une [module d'horloge temps réel DS1307](https://wiki.seeedstudio.com/Grove-RTC/) sur un des deux ports I2C de la carte.

```
cd RIOT/tests/driver_ds1307/
make BOARD=lora-e5-dev
```

Vous pouvez vous connecter au port console de la carte.
```
TODO
```

> A noter: ce programme peut permettre de dater chaque entrée dans le journal du "data logger".

> Il existe d'autres composants d'horloge temps réel plus précis comme le PCF85063TP disponible sur le module  [Grove - High Precision RTC (Real Time Clock)](https://wiki.seeedstudio.com/Grove_High_Precision_RTC/). Il n'est pas pour l'instant disponible dans la bibliothèque des pilotes de RIOT.

## Utilisation du capteur capacitif de touché 12 voies MPR121

Liens:
* https://learn.adafruit.com/adafruit-mpr121-12-key-capacitive-touch-sensor-breakout-tutorial
* https://github.com/adafruit/Adafruit_MPR121
* https://www.sparkfun.com/datasheets/Components/MPR121.pdf

