---
layout: article
title: Exercices simples avec RIOT OS pour la carte LoRa E5 Dev
description: Exercices simples avec RIOT OS pour la carte LoRa E5 Dev
permalink: lora-e5-dev.html
key: page-aside
cover: site/assets/images/lora/seeestudio_lora_e5_dev.png
aside:
  toc: true
sidebar:
  nav: site_nav
---

La carte [LoRa E5 Dev](https://wiki.seeedstudio.com/LoRa_E5_Dev_Board/) de Seeedstudio inclut quelques périphériques comme une DEL, un bouton poussoir, un capteur de température NXP LM75, deux connecteurs Grove I2C, un connecteur Grove UART et un port RS485 en plus du brochage Arduino. Un emplacement vierge est prévu pour ajouter une mémoire flash SPI.

Les exercices suivants peuvent s'effectuer:
* soit avec la carte [LoRa E5 Dev](https://wiki.seeedstudio.com/LoRa_E5_Dev_Board/) seule,
* soit avec des [modules Grove](https://wiki.seeedstudio.com/Grove_System/),

*Attention: il ne faut surtout pas brancher sur les deux connecteurs Arduino des cartes compatibles Arduino comme la [carte d'extension de connection Grove](https://www.seeedstudio.com/Base-Shield-V2.html) utilisée avec la carte Nucleo WB55 et la carte Nucleo WL55JC*

Un programmateur SWD est requis pour charger le firmware sur la carte.

## Clignotement de la DEL

La carte dispose d'une DEL qui est sur le port `D5` (`GPIO`).

Construisez le firmware de test.

```bash
cd RIOT/tests/leds/
make BOARD=lora-e5-dev
```

Connectez le chargeur SWD aux broches SWD de la carte comme l'illustre l'image ci-dessous

![LoRa E5 Dev Connection SWD](site/assets/images/lora/seeestudio_lora_e5_dev_connection.png)

> Vous pouvez utiliser le flasheur détachable des cartes Nucleo et connecter les 5 premières broches du [connecteur CN4 SWD](https://www.st.com/content/ccc/resource/technical/document/user_manual/98/2e/fa/4b/e0/82/43/b7/DM00105823.pdf/files/DM00105823.pdf/jcr:content/translations/en.DM00105823.pdf):

	Pin 1: VDD_TARGET (VDD from application) --> Fil rouge
	Pin 2: SWCLK (clock)                     --> Fil jaune
	Pin 3: GND (ground)                      --> Fil noir
	Pin 4: SWDIO (SWD data input/output)     --> Fil bleu
	Pin 5: NRST (RESET of target STM32)      --> Fil gris

Chargez le firmware sur votre carte en utilisant l'application [STM32CubeProgrammer](https://www.st.com/en/development-tools/stm32cubeprog.html) fournie par ST et préalablement installée et en suivant les [instructions données par Seeedstudio](https://wiki.seeedstudio.com/LoRa_E5_Dev_Board/#23-build-the-lorawan-end-node-example).

Vous pouvez vous connecter au port console de la carte.
```
main(): This is RIOT! (Version: 2022.01-devel-217-g43bef)
On-board LED test

Available LEDs: 1

Will now light up each LED once short and twice long in a loop
```

La DEL notée `D5` à coté du connecteur USB clignote en rouge.

## Codage d'un message en code Morse

Construisez le firmware de test.

```bash
cd RIOT/tests/pkg_umorse/
make BOARD=lora-e5-dev
```

Chargez le firmware sur votre carte.

Vous pouvez vous connecter au port console de la carte.
```
Help: Press s to start test, r to print it is ready
START
main(): This is RIOT! (Version: 2022.01-devel-217-g43bef)
[START]
> Translate text of length 30 into Morse code:
"Hello RIOT-OS!
This is uMorse."
> using aligned encoding:
.... . ._.. ._.. ___ / ._. .. ___ _ ___ ...
_ .... .. ... / .. ... / .._ __ ___ ._. ... .
> encoded length=52
> using compact encoding:
.... . ._.. ._.. ___ / ._. .. ___ _ ___ ...
_ .... .. ... / .. ... / .._ __ ___ ._. ... .
> encoded length=25
[SUCCESS]
```

## Exercice : Transmission en code Morse via la DEL 

Modifiez le programme de test de `pkg_umorse` afin d'emettre le message luninaux en Morse de la DEL `D0`.

## Utilisation des deux boutons utilisateur

La carte dispose de deux boutons utilisateur notés `D0` (GPIO `PA0`) et `BOOT` (GPIO `PB13`).

Contruisez le firmware de test:

```bash
cd RIOT/tests/buttons/
make BOARD=lora-e5-dev
```

Flashez la carte avec le programmateur SWD.

Vous pouvez vous connecter au port console de la carte et appuyer sur les deux boutons notés `D0` (GPIO `PA0`) et `BOOT` (GPIO `PB13`).

```
Help: Press s to start test, r to print it is ready
START
main(): This is RIOT! (Version: 2022.01-devel-217-g43bef)
On-board button test

 -- Available buttons: 2

 -- Try pressing buttons to test.

[SUCCESS]
Pressed BTN0
Pressed BTN1
Pressed BTN0
Pressed BTN1
```

## Affichage d'un message sur un afficheur LCD

Connectez le module [Grove LCD 16X2](https://wiki.seeedstudio.com/Grove-16x2_LCD_Series/) à un des deux ports Grove I2C de la carte

```
TODO
```

## Lecture de la température avec le capteur NXP LM75 de la carte

La carte dispose d'un capteur NXP LM75 qui est connecté au bus I2C de la MCU

```bash
cd RIOT/tests/driver_lm75/
make BOARD=lora-e5-dev DRIVER=lm75a
```

Vous pouvez vous connecter au port console de la carte.
```
main(): This is RIOT! (Version: 2022.01-devel-429-g70744)                       
Sensor test...                                                                  
Initialization...                                                               
Initialization succeeded                                                        
error setting Hyst and/or OS temps                                              
Set OS temp is 80.0ºC                                                          
Set HYST temp is 75.0ºC                                                        
20.0ºC                                                                         
Set OS temp is 80.0ºC                                                          
Set HYST temp is 75.0ºC                                                        
lm75: OS alert pin not connected or defined                                     
Error reading OS pin state                                                      
22.500ºC                                                                       
```


## Lecture des capteurs SAUL

SAUL ([_Sensor Actuator Uber Layer_](https://doc.riot-os.org/group__drivers__saul.html)) est une interface générique actionneur/capteur dans RIOT. Son objectif est de permettre une interaction unifiée avec une large gamme de capteurs et d'actionneurs via un ensemble de fonctions d'accès définies et une structure de données commune.

Chaque pilote de périphérique (_driver_) implémentant cette interface doit exposer un ensemble de fonctions prédéfinies et il doit s'enregistrer dans le registre central de SAUL. À partir de là, les appareils peuvent être trouvés, répertoriés et accessibles.

La carte `lora-e5-dev` embarque plusieurs pilotes implémentant cette interface. 

```bash
cd RIOT/tests/saul/
make BOARD=lora-e5-dev
```

```
##########################
 
Dev: LED(red)	Type: ACT_SWITCH
Data:	              1 
 
Dev: Button(B1 Boot)	Type: SENSE_BTN
Data:	              0 
 
Dev: Button(B2 D0)	Type: SENSE_BTN
Data:	              0 
 
Dev: lm75	Type: SENSE_TEMP
Data:	          20.87 °C
```

## Pour aller plus loin

## Galerie

![LoRa E5 Dev](site/assets/images/lora/seeestudio_lora_e5_dev.png)

![LoRa E5 Dev Spec](site/assets/images/lora/seeestudio_lora_e5_dev_spec.png)

![LoRa E5 Dev Pinout](site/assets/images/lora/seeestudio_lora_e5_dev_pinout.png)

![LoRa E5 Dev Connection SWD](site/assets/images/lora/seeestudio_lora_e5_dev_connection.png)
