# TODO List

## Section MicroPython / RIOTOS

* Ajouter des `pkg` et `driver` au firmware MicroPython sur RIOT
    * `pkg/semtech-loramac`
    * `driver/lm75`
    * `driver/ccs811`
    * `driver/bme680`
    * MPR121
    * Panasonic Grid-EYE (AMG88xx)
    * VL53L0X
    * VL53L5CX Time-of-Flight 8x8 multizone ranging sensor

## Section RIOT
Ajouter les cartes chinoises suivantes:

### CCS881 (eCO2, VOC)
 —> supporté par RIOT

### GY-61 avec ADXL335 (Analogique)
 —> supporté

le GY-61 est un accéléromètre 3 axes (ADXL335) qui a des sorties analogiques. Il mesure des accélérations sur une plage de ±3 g et la gravité (accélération statique) en supposant que vous ayez branché X_OUT sur A2, Y_OUT sur A1, Z_OUT sur A0

### GY-65 avec BMP085 (I2C)
 —> non supporté `cd tests/driver_bmx055; make BOARD=nucleo-wl55jc` 

### MPU6050 (I2C)
 —> non supporté `cd tests/driver_mpu9x50; make BOARD=nucleo-wl55jc`

### MMA7361 (I2C)
 —> non supporté `cd tests/driver_mma7660; make BOARD=nucleo-wl55jc`

### GY-273 avec HMC5883 (I2C)
 —> non monté `cd tests/driver_hmc5883l; make BOARD=nucleo-wl55jc`

### GY-68 avec BMP180 (I2C)
 —> non monté `cd tests/driver_bmp180; make BOARD=nucleo-wl55jc`

### BME280 (I2C)
 —> non monté `cd tests/driver_bmx280; make BOARD=nucleo-wl55jc`

### MAG3110 (I2C)
 —> non monté `cd tests/driver_mag3110; make BOARD=nucleo-wl55jc`

### VL53L0X (I2C)
—> non monté (ToF)

### VL53L5CX Time-of-Flight 8x8 multizone ranging sensor
https://www.st.com/en/imaging-and-photonics-solutions/vl53l5cx.html

### MPR121 (I2C)
 —> non monté (12 x capacity touch sensor —> tu fais un piano avec des bananes)
Dispo : version Raspberry Pi Hat

### GY-906 avec Melexis MLX90614 IR temperature sensor
* https://www.melexis.com/en/product/mlx90614/digital-plug-play-infrared-thermometer-to-can

### Panasonic Grid-EYE AMG 88xx thermal sensor 8x8 pixels (I2C)
* https://industry.panasonic.eu/components/sensors/industrial-sensors/grid-eye/amg88xx-high-performance-type/amg8833-amg8833
* https://na.industrial.panasonic.com/products/sensors/sensors-automotive-industrial-applications/lineup/grid-eye-infrared-array-sensor

### Melexis FIR MLX90640 thermal sensor 32x24 pixels (I2C)
* https://www.digikey.fr/fr/product-highlight/m/melexis/mlx90640-fir-sensor
* https://www.adafruit.com/product/4407
* https://github.com/adafruit/Adafruit_MLX90640




## Autres modules Grove intéressants

* [Grove - Formaldehyde Sensor](https://wiki.seeedstudio.com/Grove-Formaldehyde-Sensor/) contient un composant Sensirion SFA3X  
* [Grove - Doppler Radar](https://wiki.seeedstudio.com/Grove-Doppler-Radar/)
* [Grove - GPS (Air530)](https://wiki.seeedstudio.com/Grove-GPS-Air530/) dans [GPS Grove](https://wiki.seeedstudio.com/GPS-Modules-Selection-Guide/)
* [Grove - Human Presence Sensor (AK9753)](https://wiki.seeedstudio.com/Grove-Human_Presence_Sensor-AK9753/)
* [Grove - Q Touch Sensor](https://wiki.seeedstudio.com/Grove-Q_Touch_Sensor/)
* Modules chinois avec un [composant Aosong ASAIR AHT1x/AHT2x](https://github.com/enjoyneering/AHTxx)
