---
layout: article
title: Exercices simples avec RIOT OS pour la carte LoRa E5 Mini
description: Exercices simples avec RIOT OS pour la carte LoRa E5 Mini
permalink: lora-e5-mini.html
key: page-aside
cover: site/assets/images/lora/seeestudio_lora_e5_mini.png
aside:
  toc: true
sidebar:
  nav: site_nav
---

La carte [LoRa E5 Mini](https://wiki.seeedstudio.com/LoRa_E5_mini/) de Seeedstudio n'inclut qu'une DEL et un bouton utilisateur. Elle est plus compacte et peut donc être utilisée dans des projets contraints par la taille (par exemple suiveur GPS ...)

Les exercices suivants s'effectuent avec la carte [LoRa E5 Mini](https://wiki.seeedstudio.com/LoRa_E5_mini/), une plaque d'essai pour raccorder les capteurs aux broches de la carte (`UART0`, `UART1`, `UART2`, `I2C`, `SPI`, `A3`, `A4`, `D0`, `D9` ou `D10`).

Un programmateur SWD est requis pour charger le firmware sur la carte.

## Clignotement de la DEL

Construisez le firmware de test.

```bash
cd RIOT/tests/leds/
make BOARD=lora-e5-dev
```

> On réutilise pour le moment la définition de la carte LoRa E5 Dev (`BOARD=lora-e5-dev`).

Connectez le chargeur SWD aux broches SWD de la carte comme l'illustre l'image ci-dessous

![LoRa E5 Mini Connection SWD](site/assets/images/lora/seeestudio_lora_e5_mini_connection.png)

> Vous pouvez utiliser le flasheur détachable des cartes Nucleo et connecter les 5 premières broches du [connecteur CN4 SWD](https://www.st.com/content/ccc/resource/technical/document/user_manual/98/2e/fa/4b/e0/82/43/b7/DM00105823.pdf/files/DM00105823.pdf/jcr:content/translations/en.DM00105823.pdf):

	Pin 1: VDD_TARGET (VDD from application) --> Fil rouge
	Pin 2: SWCLK (clock)                     --> Fil jaune
	Pin 3: GND (ground)                      --> Fil noir
	Pin 4: SWDIO (SWD data input/output)     --> Fil bleu
	Pin 5: NRST (RESET of target STM32)      --> Fil gris

Chargez le firmware sur votre carte en utilisant l'application [STM32CubeProgrammer](https://www.st.com/en/development-tools/stm32cubeprog.html) fournie par ST et préalablement installée et en suivant les [instructions données par Seeedstudio](https://wiki.seeedstudio.com/LoRa_E5_Dev_Board/#23-build-the-lorawan-end-node-example).

Vous pouvez vous connecter au port console de la carte.
```
main(): This is RIOT! (Version: 2022.01-devel-217-g43bef)
On-board LED test

Available LEDs: 1

Will now light up each LED once short and twice long in a loop
```

La DEL notée `D5` à coté du connecteur USB clignote en rouge.


## Exercice : Transmission en code Morse via la DEL 

Modifiez le programme de test de `pkg_umorse` afin d'emettre le message luninaux en Morse de la DEL `D0`.

## Utilisation du bouton utilisateur

Construisez le firmware de test.

```bash
cd RIOT/tests/buttons/
make BOARD=lora-e5-dev
```

Vous pouvez vous connecter au port console de la carte et appuyer sur le bouton `BOOT` (GPIO `PB13`).
```
Help: Press s to start test, r to print it is ready
START
main(): This is RIOT! (Version: 2022.01-devel-217-g43bef)
On-board button test

 -- Available buttons: 2

 -- Try pressing buttons to test.

[SUCCESS]
Help: Press s to start test, r to print it is ready
START
main(): This is RIOT! (Version: 2022.01-devel-217-g43bef)
On-board button test

 -- Available buttons: 2

 -- Try pressing buttons to test.

[SUCCESS]
Pressed BTN0
Pressed BTN0
Pressed BTN0
```


## Exercice : Utilisez un capteur I2C BME280 de Bosch

Le capteur I2C BME280 de Bosch est un capteur très populaire qui mesure la température, l'humidité relative et la pression.

Construisez et chargez le firmware :

```bash
cd ~/github/RIOT-OS/RIOT
cd tests/driver_bmx280
export CFLAGS=-DBMX280_PARAM_I2C_ADDR=0x76
BOARD=lora-e5-dev gmake -j 8 
BOARD=lora-e5-dev gmake -j 8 flash-only
```

La trace est :
```
BMX280 test application

+------------Initializing------------+
Initialization successful

+------------Calibration Data------------+
dig_T1: 28273
dig_T2: 26729
dig_T3: 50
dig_P1: 36771
dig_P2: -10503
dig_P3: 3024
dig_P4: 7024
dig_P5: -4
dig_P6: -7
dig_P7: 9900
dig_P8: -10230
dig_P9: 4285
dig_H1: 75
dig_H2: 383
dig_H3: 0
dig_H4: 271
dig_H5: 50
dig_H6: 30

+-------------------------------------+

Temperature [°C]: 24.81
   Pressure [Pa]: 97945
  Humidity [%rH]: 42.63

+-------------------------------------+

Temperature [°C]: 24.81
   Pressure [Pa]: 97944
  Humidity [%rH]: 42.07
```


## Pour aller plus loin



## Galerie

![LoRa E5 Mini](site/assets/images/lora/seeestudio_lora_e5_mini.png)

![LoRa E5 Mini Spec](site/assets/images/lora/seeestudio_lora_e5_mini_spec.png)

![LoRa E5 Mini Pinout](site/assets/images/lora/seeestudio_lora_e5_mini_pinout.png)

![LoRa E5 Mini Connection SWD](site/assets/images/lora/seeestudio_lora_e5_mini_connection.png)
