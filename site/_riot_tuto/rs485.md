---
layout: article
title: Communication via le port 485 
description: Communication via le port 485
permalink: riot_rs485.html
key: page-aside
cover: site/assets/images/lora/rs485.png
aside:
  toc: true
sidebar:
  nav: site_nav
---
## Communication avec un autre équipement via le port RS485

Le RS485 est une norme de transmission de données série très largement utilisée dans l'industrie. Il offre un débit de 10Mbit/s et jusqu'à 1200 mètres de distance à bas débit. Le protocole Modbus est souvent utilisé lors de la mise en place d'une communication RS485. [Pour en savoir plus](https://www.virtual-serial-port.org/fr/articles/modbus-vs-rs485/).

La carte LoRa E5 dispose d'un port RS485.

```
TODO
```

> [Exemple de montage avec une autre carte](https://create.arduino.cc/projecthub/maurizfa-13216008-arthur-jogy-13216037-agha-maretha-13216095/modbus-rs-485-using-arduino-c055b5)

