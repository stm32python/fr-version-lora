---
layout: article
title: Communication série en utilisant les UART 
description: Communication série en utilisant les UART
permalink: riot_uart_gps.html
key: page-aside
cover: site/assets/images/stm32duino/grove-gps.jpg
aside:
  toc: true
sidebar:
  nav: site_nav
---
## Communication série en utilisant les UART

La carte LoRa E5 Dev possède 3 ports UART sont:
* le port (`PB7`,`PB6`) qui se trouve à la fois sur le connecteur Grove `UART`, sur le connecteur `USB->TTL` et sur les broches `RX` `TX` du connecteur `J2` (emplacements 4,5)
* le port (`PA3`,`PA2`) console qui se trouve à la fois sur le connecteur J12 `LPUART` et sur les broches `RX1` `TX2` du connecteur `J7` (emplacements 1,2)
* le port (`PC1`,`PC0`) console qui se trouve uniquement sur les broches `RX2` `TX2` du connecteur `J2` (emplacements 8,7)

Le premier port est rélié à la console standard (`STDIO_UART`).

> A noter, si vous voulez modifier le baudrate de la console standard, vous devez compiler le programme avec une autre valeur de `STDIO_UART_BAUDRATE`: il faut ajouter la ligne `CFLAGS += -DSTDIO_UART_BAUDRATE=9600` dans le `Makefile`.

Testez les communications entre les deux autres ports en croisant deux fils branchés sur `TX1 <--> RX2` et `TX1 <--> RX2`.

```bash
cd RIOT/tests/periph_uart/
gmake BOARD=lora-e5-dev
```

```
UARD_DEV(0): test uart_poweron() and uart_poweroff()  ->  [OK]

UART INFO:
Available devices:               3
UART used for STDIO (the shell): UART_DEV(0)

> help
Command              Description                                                
---------------------------------------                                         
init                 Initialize a UART device with a given baudrate             
mode                 Setup data bits, stop bits and parity for a given UART deve
send                 Send a string through given UART device                    
test                 Run an automated test on a UART with RX and TX connected   
> init 1 9600                                                                   
Success: Initialized UART_DEV(1) at BAUD 9600                                   
UARD_DEV(1): test uart_poweron() and uart_poweroff()  ->  [OK]                  
> init 2 9600                                                                   
Success: Initialized UART_DEV(2) at BAUD 9600                                   
UARD_DEV(2): test uart_poweron() and uart_poweroff()  ->  [OK]                  
> send 2 ABCDE                                                                  
UART_DEV(2) TX: ABCDE                                                           
>Success: UART_DEV(1) RX: [ABCDE]\n                                             
 send 1 FGHIJ                                                                    
UART_DEV(1) TX: FGHIJ                                                            
Success: UART_DEV(2) RX: [FGHIJ]\n                                               
```

## Lecture de la position GPS

Connectez le module [Grove GNSS](https://wiki.seeedstudio.com/Grove-GPS) au port LPUART de la carte.

> Le baudrate par défaut du module Grove GPS équipé avec un SIM28 est 9600. Attention. Le baudrate est de 4800 pour les modeles avec un uBlox.

```bash
cd RIOT/tests/pkg_minmea/
make BOARD=lora-e5-dev
```

Modifiez le fichier `Makefile` du test `periph_uart`
```makefile
USEPKG += minmea
USEMODULE += fmt
CFLAGS += -Wno-switch
```

Modifiez le fichier `main.c` du test `periph_uart` au niveau de la fonction `rx_cb()`
```c
static void parse_nmea_gll(const char * sentence){
	struct minmea_sentence_gll frame;
	int res = minmea_parse_gll(&frame, sentence);
	if (!res) {
		print_str("FAILURE: error parsing GPS GLL sentence\n");
	} else {
		print_str("parsed coordinates: lat=");
		print_float(minmea_tocoord(&frame.latitude), 6);
		print_str(" lon=");
		print_float(minmea_tocoord(&frame.longitude), 6);
		print_str("\nSUCCESS\n");
	}
}

#define NMEA_BUFFER_SIZE 256
static uint8_t nema_buffer[NMEA_BUFFER_SIZE];
static int nema_buffer_idx = 0;

static void parse_nmea(uint8_t data)
{
    if (data == '\n') {

    	 // Determine sentence identifier.
       	enum minmea_sentence_id s = minmea_sentence_id((const char *)nema_buffer, true);
       	switch(s) {
       	case MINMEA_SENTENCE_GLL: // $GPGLL Sentence (Position)
       		parse_nmea_gll((const char *)nema_buffer);
       		break;
       	case MINMEA_SENTENCE_RMC: // $GPRMC Sentence (Position and time)
       	case MINMEA_SENTENCE_GGA: // $GPGGA Sentence (Fix data)
       	case MINMEA_SENTENCE_GSA: // $GPGSA Sentence (Active satellites)
       	case MINMEA_SENTENCE_GST: // $GPGST Sentence (pseudorange noise statistics)
       	case MINMEA_SENTENCE_GSV: // $GPGSV Sentence (Satellites in view)
       	case MINMEA_SENTENCE_VTG: // $GPVTG Sentence (Course over ground)
       	case MINMEA_SENTENCE_ZDA: // $GPZDA Sentence (UTC and local date/time data)
			{
		        printf("TODO: parse NMEA sentence %d.\n", s);
			}
       		break;
       	case MINMEA_INVALID:
       	case MINMEA_UNKNOWN:
       		break;
       	default:
       		break;
       	}
        nema_buffer_idx = 0;
    } else  if (data == '\r') {
		// Skip
	} else {
    	if(nema_buffer_idx < NMEA_BUFFER_SIZE) {
    		nema_buffer[nema_buffer_idx] = data;
    		nema_buffer_idx++;
    		nema_buffer[nema_buffer_idx] = '\0';
    	}
    }
}


static void rx_cb(void *arg, uint8_t data)
{
    uart_t dev = (uart_t)arg;

    ringbuffer_add_one(&ctx[dev].rx_buf, data);
    // Add the char for parsing the potential NMEA sentence
    parse_nmea(data);
    if (!test_mode && data == '\n') {
        msg_t msg;
        msg.content.value = (uint32_t)dev;
        msg_send(&msg, printer_pid);
    }
}

```

```bash
cd RIOT/tests/periph_uart/
gmake BOARD=lora-e5-dev
```

Si vous ne possédez pas de module, vous pouvez tester en croissant les fils comme à l'exercice précédent et en envoyant la phrase NMEA GLL `$GNGLL,5229.0178,N,01326.7605,E,114350.000,A,A*45` à la deuxième UART.

```
UARD_DEV(0): test uart_poweron() and uart_poweroff()  ->  [OK]

UART INFO:
Available devices:               3
UART used for STDIO (the shell): UART_DEV(0)

> help
Command              Description                                                
---------------------------------------                                         
init                 Initialize a UART device with a given baudrate             
mode                 Setup data bits, stop bits and parity for a given UART deve
send                 Send a string through given UART device                    
test                 Run an automated test on a UART with RX and TX connected   
> init 1 9600                                                                   
Success: Initialized UART_DEV(1) at BAUD 9600                                   
UARD_DEV(1): test uart_poweron() and uart_poweroff()  ->  [OK]                  
> init 2 9600                                                                   
Success: Initialized UART_DEV(2) at BAUD 9600                                   
UARD_DEV(2): test uart_poweron() and uart_poweroff()  ->  [OK]                  
> send 2 TEST                                                                   
UART_DEV(2) TX: TEST                                                            
Success: UART_DEV(1) RX: [TEST]\n                                               
> send 1 $GNGLL,5229.0178,N,01326.7605,E,114350.000,A,A*45
UART_DEV(1) TX: $GNGLL,5229.0178,N,01326.7605,E,114350.000,A,A*45               
parsed coordinates: lat=52.483631 lon=13.446008                                 
SUCCESS                                                                         
Success: UART_DEV(2) RX: [$GNGLL,5229.0178,N,01326.7605,E,114350.000,A,A*45]\n  
> send 1 $GPGGA,161229.487,3723.2475,N,12158.3416,W,1,07,1.0,9.0,M,,,,0000*18
UART_DEV(1) TX: $GPGGA,161229.487,3723.2475,N,12158.3416,W,1,07,1.0,9.0,M,,,,0000*18
TODO: parse NMEA sentence 2.
Success: UART_DEV(2) RX: [$GPGGA,161229.487,3723.2475,N,12158.3416,W,1,07,1.0,9.0,M,,,,0000*18]\n
> send 1 $GPRMC,161229.487,A,3723.2475,N,12158.3416,W,0.13,309.62,120598,,*10 
UART_DEV(1) TX: $GPRMC,161229.487,A,3723.2475,N,12158.3416,W,0.13,309.62,120598,,*10
TODO: parse NMEA sentence 1.
Success: UART_DEV(2) RX: [$GPRMC,161229.487,A,3723.2475,N,12158.3416,W,0.13,309.62,120598,,*10]\n
> send 1 $GPGSV,1,1,01,21,00,000,*4B
UART_DEV(1) TX: $GPGSV,1,1,01,21,00,000,*4B
TODO: parse NMEA sentence 6.
Success: UART_DEV(2) RX: [$GPGSV,1,1,01,21,00,000,*4B]\n
> send 1 $GPGSV,3,1,10,20,78,331,45,01,59,235,47,22,41,069,,13,32,252,45*70
UART_DEV(1) TX: $GPGSV,3,1,10,20,78,331,45,01,59,235,47,22,41,069,,13,32,252,45*70
TODO: parse NMEA sentence 6.
Success: UART_DEV(2) RX: [$GPGSV,3,1,10,20,78,331,45,01,59,235,47,22,41,069,,13,32,252,45*70]\n
```

Terminez le traitement des autres phrases NMEA. Vous pouvez essayer d'[autres exemples de phrases NMEA](https://www.satsleuth.com/GPS_NMEA_sentences.aspx) pour mettre au point votre programme.

> Il existe des modules GNSS beaucoup plus performant que le module SIM-28 du module Grove.

> A noter: ce programme est l'embryon d'un suiveur (véhicule, ballon stratosphérique ...).

