---
layout: article
title: STM32duino
description: Comment installer STM32duino
permalink: installation_duino.html
key: page-aside
cover: site/assets/cover/install.jpg
aside:
  toc: true
sidebar:
  nav: site_nav
---

## Installation de l'environnement Arduino
Dans un premier temps il est nécessaire d'utiliser l'environnement de développement Arduino.

Télécharger et installer Arduino IDE depuis le lien [https://www.arduino.cc/en/Main/Software](https://www.arduino.cc/en/Main/Software)

### Installation de le package STM32Duino
Une fois Arduino IDE installé, Lancez Arduino IDE puis allez dans `Fichier > Préférences`

Une fenêtre s'ouvre :

![Image](site/assets/images/stm32duino/install_1.png)


Dans `URL de gestionnaire de cartes suplémentaires`, entrez l'URL suivante:
`https://github.com/stm32duino/BoardManagerFiles/raw/main/package_stmicroelectronics_index.json`

Entrez par la suite "OK"

Ensuite : `Outils > Type de cartes : > Gestionnaire de cartes`
![Image](site/assets/images/stm32duino/install_2.png)

Entrez dans la barre de recherche "STM32" ou "stm" et télécharger le package.
![Image](site/assets/images/stm32duino/install_3.png)

Cliquez sur installer.
![Image](site/assets/images/stm32duino/install_4.png)

> Les instructions complémentaires peuvent être trouvés [ici](https://github.com/stm32duino/Arduino_Core_STM32).

### Installation de l'environnement STM32CubeIDE

Pour pouvoir utiliser votre STM32 avec l"environnement Arduino, vous devez installer l'outil STM32CubeIDE.
Telecharger et installer STM32CubeIDE disponible a cette adresse [https://www.st.com/en/development-tools/stm32cubeide.html
](https://www.st.com/en/development-tools/stm32cubeide.html).

> Attention : il est nécessaire de créer un compte my.st.com

Lancez arduino IDE avec un STM32 branché en USB sur l'ordinateur.

Pour une carte Nucleo 64, sélectionnez dans `Outils > Type de cartes` et choisissez `STM32 boards groupes > Nucleo-64` comme le montre l'image ci-dessous.

![Image](site/assets/images/stm32duino/install_5.png)

Sélectionnez ensuite dans `Outils > Board Part Number` la carte STM32 correspondante
à votre matériel:  `Nucleo F446RE` (dans notre exemple), `Nucleo WL55JC1` ou `P-Nucleo WB55`.

![Image](site/assets/images/stm32duino/install_6.png)


Pour une carte Discovery, sellectionnez dans `Outils > Type de cartes > STM32 boards groupes > Discovery`

Sélectionnez ensuite dans `Outils > Board Part Number` et choisissez la carte STM32 correspondante
à votre matériel:  `B-L072Z-LRWAN1`, `B-L475E-IOT01A`, `B-U585I-IOT02A` ...

![Image](site/assets/images/stm32duino/install_6.png)


Dans `Outils > Upload method` :

![Image](site/assets/images/stm32duino/install_7.png)

Ensuite : `Outils > Port`: et choisissez le port COM correspondant a votre périphérique STM32.

![Image](site/assets/images/stm32duino/install_8.png)


Vous pouvez alors programmer votre carte STM32.

Félicitation !
Ce tutoriel est términé.
