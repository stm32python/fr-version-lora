---
layout: article
title: Afficheur LCD RGB 16 caractères x 2 lignes
description: Mise en oeuvre un afficheur LCD RGB 16 caractères x 2 lignes avec MicroPython
permalink: LCD_16x2.html
key: page-single
cover: site/assets/cover/Display_grove_I2C.jpg

aside:
  toc: true
sidebar:
  nav: site_nav
---


# Mise en oeuvre de l'afficheur LCD RGB 16x2

Ce tutoriel explique comment mettre en oeuvre un afficheur I2C LCD RGB 16 caractères x 2 lignes avec MicroPython.

**Matériel requis :**
1. NUCLEO-WB55
2. Carte Grove Base Shield
3. LCD RGB Grove **5V**
4. Carte X-NUCLEO IKS01A3

**Le LCD RGB :**

<div align="center">
<img alt="Grove - RGB LCD" src="images/lcb_rgb_grove.png" width="365px">
</div>

**Objectif de l'exercice**
- Mesurer la température (en degrés Celsius) de l'air ambiant toutes les secondes
- Afficher la température sur le LCD RGB Grove et sur le terminal de l'USB USER
- Ajuster la couleur de fond du LCD selon la température mesurée

**Mise en oeuvre**
- Connecter la carte **Grove Base Shield pour Arduino** sur la NUCLEO-WB55
- Connecter l'afficheur sur **I2C1** et **pensez à mettre de commutateur VCC de Grove Base Shield sur 5V !** Autrement, le LCD ne s'allumera pas.
- Connecter la carte X-NUCLEO-IKS01A3 sur la carte NUCLEO-WB55
- Copier les fichiers (librairies) *i2c_lcd_screen.py*, *i2c_lcd_backlight.py*, *i2c_lcd* ([source](https://github.com/Bucknalla/micropython-i2c-lcd)) et STTS751 dans le dossier PYBFLASH.
- Copier le code suivant dans le fichier *main.py* :

**Le code MicroPython**
```python
# Classes pour le LCD RGB copiées depuis ce site : https://github.com/Bucknalla/micropython-i2c-lcd
# Objet du script :
# Mesure la température (en degré Celsius) de l'air ambiant toutes les secondes
# Affiche la température sur le LCD RGB Grove et sur le terminal de l'USB USER
# Ajuste la couleur de fond du LCD et allume les LED selon la température
# Cet exemple nécessite un shield X-NUCLEO IKS01A3, un Grove Base Shield pour Arduino et un LCD RGB Grove.
# Attention, le LCD RGB Grove doit être alimenté en 5V, pensez à placer le commutateur du Grove Base Shield
# poiur Arduino sur la bonne position !
# NB : C'est le shield X-NUCLEO IKS01A3 qui apporte les résistances de PULL-UP sur les broches
# SCL et SDA de l'I2C nécessaires au bon fonctionnement du LCD RGB Grove.

from machine import I2C
import STTS751
import pyb
import time
import i2c_lcd

# On utilise l'I2C n°1 de la carte NUCLEO-W55 pour communiquer avec le capteur
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
time.sleep_ms(1000)

sensor = STTS751.STTS751(i2c)

#Instance de la classe Display
lcd = i2c_lcd.Display(i2c)
lcd.home()

led_bleu = pyb.LED(1)
led_vert = pyb.LED(2)
led_rouge = pyb.LED(3)

while True:

	# On arrondit la température mesurée à 1 décimale
	temp = round(sensor.temperature(),1)

	print("Température : " + str(temp) + "°C (", end='')

	lcd.move(0,0)
	lcd.write('Temperature (C)')
	lcd.move(0,1)
	lcd.write(str(temp))
	
	if temp > 25 :
		led_rouge.on()
		print("chaud)")
		lcd.color(255,0,0) # Rétroéclairage du LCD : rouge
	elif temp > 18 and temp <= 25 :
		led_vert.on()
		print("confortable)")
		lcd.color(0,255,0) # Rétroéclairage du LCD : vert
	else:
		led_bleu.on()
		print("froid)")
		lcd.color(0,0,255) # Rétroéclairage du LCD : bleu

	led_rouge.off()
	led_vert.off()
	led_bleu.off()
	
	time.sleep_ms(1000)
```

> Crédit image : [Seeedstudio](http://wiki.seeedstudio.com/Grove_System/)
