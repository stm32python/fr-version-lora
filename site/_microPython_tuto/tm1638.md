---
layout : article
title: Afficheur 8x7-segments TM1638
description: Exercice avec afficheur 8x7-segments TM1638 en MicroPython
permalink: tuto_afficheur_8x7s.html
key: page-aside
cover: site/assets/cover/Display_I2C.jpg
aside:
  toc: true
sidebar:
  nav: site_nav
---
## Montage

La connectique de l'afficheur s'organise de la manière suivante :  
![tm1638-schema.png](site/assets/images/microPython/tm1638-schema.png)
Vous pouvez retrouver les informations de connectique à l'arrière de la carte.


| Afficheur         | ST Nucleo         |
| :-------------:   | :---------------: |
|      5V           |         5V        |
|      DIO          |         D4        |
|      CLK          |         D3        |
|      STB0         |         D2        |
|      GND          |         GND       |

## Utilisation

En premier, il est nécessaire d'importer les bibliothèques :  

```python
# Importation des bibliothèques
import tm1638
from machine import Pin
```

Ensuite on initialise les pin de communication avec la carte :  

```python
# Déclaration de la carte
tm = tm1638.TM1638(stb=Pin('D2'), clk=Pin('D3'), dio=Pin('D4'))
```

Maintenant la carte est prête à être utilisée et voici les instructions pour utiliser les différentes parties :  

- Les LEDS :  
`tm.leds(valeur)` : allume/éteint les leds sont la valeur indiquée  
`tm.led(position, valeur)` : allume/éteint la led à la position indiquée ex :  

```python
# Gestion des leds
tm.leds(0b01010101) # allume une led sur 2
tm.leds(0b00000001) # allume la led 1 et éteint les autres
tm.led(2, 0)	# éteint la led 3
tm.led(0,1) 	# allume la led 1
```

- Les boutons :  
`tm.keys()` renvoie un mot binaire avec 1 pour indiquer que le bouton est pressé. Par exemple si tm.keys() renvoie 0b00100001, cela signifie que les 1er et 6e boutons sont pressés.  

```python
# on récupère l'information sur les boutons
boutons = tm.keys()
```

- Les segments :  
`tm.show('mot')` : permet d'écrire les caractères 0-9, A-Z, espace, -, ° (affiché en utilisant '*') et les points. L'affichage est aligné à gauche et n'efface pas ce qui à été précédemment affiché si le texte est plus petit que les 8 caractères.  
`tm.number(nombre)` : permet d'afficher un entier. L'affichage est aligné à droite et efface les cases non-remplies.  
`tm.scroll('message', temps)` : permet d'afficher un message qui se déplace de droite à gauche. "temps" est optionel et permet de changer la vitesse du message.  

```python
# Segments
tm.show('  *--*  ')
tm.show('a.b.c.d.e.f.g.h.')

tm.number(-1234567)
tm.number(1234)

tm.scroll('Hello World')
tm.scroll('Bonjour tout le monde', 100)
```

- Ajustement de la luminosité :  
Il est possible de modifier la luminosité des leds et segments avec `tm.brightness(0)` ou de les éteindre avec `tm.clear()`.  

```python
# Change la luminosité des leds et segments
tm.brightness(0)

# Eteint les leds et segments
tm.clear()
```

[La Bibliothèque TM1638](https://github.com/mcauser/micropython-tm1638)
