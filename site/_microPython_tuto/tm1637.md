---
layout: article
title: Afficheur 4x7-segments TM1637
description: Mise en oeuvre de l'afficheur 7 segments 4 digits TM1637 avec MicroPython
permalink: TM1637.html
key: page-single
cover: site/assets/cover/Display_grove_I2C.jpg

aside:
  toc: true
sidebar:
  nav: site_nav
---


Ce tutoriel explique comment mettre en oeuvre un afficheur 7 segments 4 digits TM1637 avec MicroPython.

**Matériel requis :**

1. Une carte d'extension de base Grove
2. La carte NUCLEO-WB55
3. Un afficheur 4x7-segment Grove

**L'afficheur 4x7-segments Grove :**

<div align="center">
<img alt="Grove - 4-Digit Display" src="site/assets/images/microPython/grove-4_digit_display.jpg" width="400px">
</div>

Pour utiliser l'afficheur, en plus des bibliothèques **pyb** et **time** déjà présentes dans l'interpréteur MicroPython, il vous faudra également installer celle qui gère le tm1637. Déplacez le fichier **tm1637.py** dans le dossier où se trouve **main.py**.
La bibliothèque est installée ! Vous pouvez à présent utiliser ces deux bibliothèques avec :

```
from pyb import Pin
import tm1637
```
Vous allez définir votre accès à l'afficheur sur **le connecteur Grove A0** avec l'instruction suivante :

```
tm = tm1637.TM1637(clk=Pin('A0'), dio=Pin('A1'))
```

Vous pouvez maintenant modifier l'affichage en utilisant :
```
tm.write([<case1>, <case2>, <case3>, <case4>])
```
Il est possible de contrôler l’affichage des 7 segments indépendamment avec un mot binaire de la forme 0bABCDEFG.
Mettez simplement un 1 ou un 0 selon que vous voulez allumer ou éteindre le segment.
Par exemple, pour afficher 0123, écrivez :
```
tm.write([0b0111111, 0b0000110, 0b1011011, 0b1001111])
```

![Afficheur 7 segments Label](site/assets/images/microPython/300px-7_segment_display_labeled.jpg)

Vous trouverez ci-après un exemple un peu plus élaboré qui affiche la température mesurée avec le capteur STTS751 du shield X-NUCLEO IKS01A3.
L'affichage est géré de façon très simple grâce à la méthode _.number_ de la classe tm1637 ([source](https://github.com/mcauser/micropython-tm1637')).

**Le code MicroPython :**

``` python
# Exemple adapté de https://github.com/mcauser/micropython-tm1637'
# Objet du script :
# Mesure la température (en degré celsius) de l'air ambiant toutes les secondes
# Affiche la température sur l'afficheur Grove 7-segments

# Cet exemple nécessite un shield X-NUCLEO IKS01A3, un Grove Base Shield pour Arduino et un Afficheur Grove 7-segments.
# L'afficheur 7-segments doit être connecté sur la fiche "A0" du Grove Base Shield pour Arduino.

from machine import I2C # Pilotes du bus I2C
import STTS751 # Pilotes du capteur de température

# Pilotes GPIO et temporisation de Micropython
import pyb
import time
from pyb import Pin
import tm1637 # Pilotes de l'afficheur 7-segments

# Initialisation de l'afficheur 7-segments
tm = tm1637.TM1637(clk=Pin('A0'), dio=Pin('A1'))

# On utilise l'I2C n°1 de la carte NUCLEO-W55 pour communiquer le capteur de température
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
time.sleep_ms(1000)

sensor = STTS751.STTS751(i2c)

led_bleu = pyb.LED(1)
led_vert = pyb.LED(2)
led_rouge = pyb.LED(3)

while True:

	temp = round(sensor.temperature()) # Arrondi à l'entier le plus proche

	print("Température : " + str(temp) + "°C (", end='')

	tm.number(temp) # Affiche la température

	if temp > 24:
		led_rouge.on()
		print("Chaud)")
	elif temp > 18 and temp <= 24:
		led_vert.on()
		print("Confortable)")
	else:
		led_bleu.on()
		print("Froid)")

	led_rouge.off()
	led_vert.off()
	led_bleu.off()
	
	time.sleep_ms(1000) # Temporisation d'une seconde
```

> Crédit image : [Wikipedia](https://fr.wikipedia.org/wiki/Fichier:7_segment_display_labeled.svg), [Seeedstudio](http://wiki.seeedstudio.com/Grove-4-Digit_Display/)
