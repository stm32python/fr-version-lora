---
layout: article
title: (avancé) Carte d'extension IKS01A3
description: Exercices avancés en MicroPython avec la carte d'extension IKS01A3
permalink: micro_python_iks01a3.html
key: page-single
cover: site/assets/cover/microPython/IKS01A3.jpg

aside:
  toc: true
sidebar:
  nav: site_nav
---

Vous devrez disposer de la carte d’extension IKS01A3 pour ces exercices.<br>
**Le code source pour chaque exercice/capteur est disponible dans la [zone de téléchargement](./telechargement)**<br>.

## Présentation

La carte d’extension IKS01A3 fait la démonstration de plusieurs capteurs MEMS (systèmes micro électro-mécaniques) de STMicroelectronics. Sa version A3 rassemble les capteurs suivants :

* [LSM6DSO](https://www.st.com/content/st_com/en/products/mems-and-sensors/inemo-inertial-modules/lsm6dso.html#tools-software) : Accéléromètre 3D + Gyroscope 3D
* [LIS2MDL](https://www.st.com/content/st_com/en/products/mems-and-sensors/e-compasses/lis2mdl.html) : Magnétomètre 3D
* [LIS2DW12](https://www.st.com/content/st_com/en/products/mems-and-sensors/accelerometers/lis2dw12.html) : Accéléromètre 3D
* [LPS22HH](https://www.st.com/content/st_com/en/products/mems-and-sensors/pressure-sensors/lps22hh.html) : Baromètre (260 à 1260 hPa)
* [HTS221](https://www.st.com/content/st_com/en/products/mems-and-sensors/humidity-sensors/hts221.html) : Capteur d’humidité relative
* [STTS751](https://www.st.com/content/st_com/en/products/mems-and-sensors/temperature-sensors/stts751.html) : Capteur de température (–40 °C à +125 °C)

Une fois la carte d'extension placée sur les connecteurs Arduino, ces capteurs sont raccordés au bus *I2C* de la carte NUCLEO-WB55.
Lorsque vous bracnchez la carte *IKS01A3*, assurez vous de bien respecter le marquage des connecteurs (ie le bon alignement avec les connecteurs Arduino) : CN9 -> CN9, CN5 -> CN5, etc.

La carte d’extension *IKS01A3* dispose également d'un emplacement au format DIL 24 broches pour y ajouter des capteurs I2C supplémentaires (par exemple, le gyroscope [A3G4250D](https://www.st.com/en/evaluation-tools/steval-mki125v1.html)).

![image](site/assets/images/microPython/iks01a3.png)


## Utilisation de l'accéléromètre LIS2DW12

Pour cette démonstration, nous avons choisi d’utiliser l'accéléromètre 3 axes (LIS2DW12). Elle consiste à allumer une LED selon l’axe sur lequel l’accélération (-1g) due à la gravité est détectée. Le vecteur accélération est décomposé sur un trièdre (x, y ,z) orthogonal.

Voici la configuration des axes de l’accélération / couleurs de LED :

*   Accélération détectée selon l'axe x : LED verte allumée
*   Accélération détectée selon l'axe y : LED bleu allumée
*   Accélération détectée selon l'axe z : LED rouge allumée

![image](site/assets/images/microPython/iks01a3-3dir.png)


Le fichier ***LIS2DW12.py*** est la bibliothèque contenant les classes pour gérer l'accéléromètre (disponible dans l'exemple [LIS2DW12](./telechargement)) et doit être copié dans le répertoire du disque USB virtuel associé à la NUCLEO-WB55 : ***PYBFLASH***.


Éditez maintenant le script ***main.py***:

``` python
# Objet du script :
# Mesure les accélérations suivant 3 axes orthogonaux (en mg) toutes les demi secondes.
# Allume ou éteint les LED de la carte selon les valeurs des accélérations
# Cet exemple nécessite un shield X-NUCLEO IKS01A3 pour l'accéléromètre MEMS LIS2DW12

from machine import I2C
import LIS2DW12
import pyb
import time

# On utilise l'I2C n°1 de la carte NUCLEO-W55 pour communiquer avec le capteur
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
time.sleep_ms(1000)

# Instanciation de l'accéléromètre
accelerometre = LIS2DW12.LIS2DW12(i2c)

led_bleu = pyb.LED(1)
led_vert = pyb.LED(2)
led_rouge = pyb.LED(3)

while True:
    time.sleep_ms(500)
    if abs(accelerometre.x()) > 700 : # Si la valeur absolue de l'accélération sur l'axe X est supérieure à 700 mg alors
        led_vert.on()
        print("led_vert.on")
    else:
        led_vert.off()
    if abs(accelerometre.y()) > 700 : # Si la valeur absolue de l'accélération sur l'axe Y est supérieure à 700 mg alors
        led_bleu.on()
        print("led_bleu.on")
    else:
        led_bleu.off()
    if abs(accelerometre.z()) > 700 : # Si la valeur absolue de l'accélération sur l'axe Z est supérieure à 700 mg alors
        led_rouge.on()
        print("led_rouge.on")
    else:
        led_rouge.off()
```

## Autres capteurs

**Vous pourrez utiliser les autres capteurs de la carte d’extension grâce aux bibliothèques et scripts exemples disponibles dans la [zone de téléchargement](./telechargement)**

A noter, les 3 capteurs suivants permettent de construire les bases d'une station météo connectée.

*  STTS751 : Capteur de température (–40 °C to +125 °C)
*  HTS221 : Capteur d’humidité relative
*  LPS22HH : Baromètre (260-1260 hPa)


## Élements de base des capteurs :  

Dans cette section sont repris pour chaque capteur :  
* Les importations de bibliothèques et déclarations du nom du capteur qui doivent se trouver en début de programme.
* Les fonctions qui permettent l'accès aux valeurs des mesures.  


### LSM6DSO - Accéléromètre et Gyroscope 3D

Déclarations initiales :  
``` python  
from machine import I2C
import LSM6DSO

i2c = I2C(1)	# On utilise l'I2C n°1 de la carte NUCLEO-W55 pour communiquer avec le capteur

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
time.sleep_ms(1000)

intertial_sensor = LSM6DSO.LSM6DSO(i2c)	# le nom du capteur
```

Mesures :  
``` python  
intertial_sensor.ax()	# résultat en mg (g intensité de la pesenteur)
intertial_sensor.ay()
intertial_sensor.az()

intertial_sensor.gx()	# résultat en rad/s
intertial_sensor.gy()
intertial_sensor.gz()
```  


### LIS2MDL - Magnétomètre 3D

Déclarations initiales :
``` python  
from machine import I2C
import LIS2MDL

i2c = I2C(1)	# On utilise l'I2C n°1 de la carte NUCLEO-W55 pour communiquer avec le capteur

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
time.sleep_ms(1000)

boussole = LIS2MDL.LIS2MDL(i2c)		# le nom du capteur
```

Mesures :  
``` python
boussole.x()	# résultat en µT
boussole.y()
boussole.z()
```  


### LPS22HH - Baromètre (260-1260 hPa) et capteur de température  

Déclarations initiales :
``` python  
from machine import I2C
import LIS2MDL

i2c = I2C(1)	# On utilise l'I2C n°1 de la carte NUCLEO-W55 pour communiquer avec le capteur

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
time.sleep_ms(1000)

boussole = LIS2MDL.LIS2MDL(i2c)		# le nom du capteur
```

Mesures :  
``` python
sensor.temperature()	# résultat en °C
sensor.pressure()		# résultat en hPa
```  


### HTS221 - Capteur de température et d'humidité relative  

Déclarations initiales :
``` python  
from machine import I2C
import HTS221

i2c = I2C(1)	# On utilise l'I2C n°1 de la carte NUCLEO-W55 pour communiquer avec le capteur

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
time.sleep_ms(1000)

sensor = HTS221.HTS221(i2c)		# le nom du capteur
```  

Mesures :  
