---
layout: article
title: Notebook Jupyter avec la carte MicroPython
description: Utilisation d'un notebook Jupyter avec la carte MicroPython
permalink: notebook.html
key: page-aside
cover: /site/assets/cover/home_page/home.jpg
aside:
  toc: true
sidebar:
  nav: site_nav
---
<div align="center"> <img  width="130" src="{{site.baseurl}}/assets/logos/logo_jupyter.png" alt="Jupyter" /></div>

# Utilisation d'un notebook Jupyter avec la carte MicroPython

 Demarrer Jupyter en entrant la commande
```bash
jupyter notebook
```

En haut à droite cliquer sur `New` et sélectionner `MicroPython-USB`

![creation notebook jupyter](site/assets/images/jupyter/jupyter_new.PNG)

 Pour utiliser la carte nous devons préciser le port à utiliser. Cette ligne est à ajouté comme première ligne du notebook :
```bash
serialconnect to –port=COM14 –baud=115200
```

> Attention, le port `COM` pourrait changer suivant l'ordinateur. Vous pouvez maitenant exécuter dans ce notebook tous les programmes décrit dans les exercices.
