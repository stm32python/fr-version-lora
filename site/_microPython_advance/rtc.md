---
layout : article
title: Real Time Clock (RTC)
description: Exercice avec Real Time Clock (RTC) en MicroPython
permalink: tuto_real_time_clock.html
key: page-aside
cover: site/assets/cover/microPython/time.jpg
aside:
  toc: true
sidebar:
  nav: site_nav
---

## Utilisation  

La RTC est assez simple à utiliser puisque seulement une methode nous servira : `pyb.RTC().datetime()`.  
La date utilisée prendra alors le format suivant :
- \[year,  m,  j,  wd,  h,  m,  s,  sub]  
- annee  mois  jour  jour_de_la_semaine  heure  minute  seconde  subsecond(compteur interne)  

Nous commencerons par donner un nom à la RTC  
```python
rtc = pyb.RTC()
```  
`rtc.datetime()` utilisée seule renvoie la date actuelle au format indiqué.  
`rtc.datetime(date)` remplace la date actuelle.  
La date du samedi 2 janvier 2021 à 12h21 51s peut alors être définie comme ceci :
```python
rtc.datetime((2021, 1, 2, 6, 12, 21, 51, 0))
```
et elle peut être récupérée comme cela :  
```python
date = rtc.datetime()
```

## Exemples  

Ainsi un premier code pour afficher sur le terminal serait :  
```python
# time permet d'attendre et temporiser
import time

# on declare la RTC (Real Time Clock)
rtc = pyb.RTC()

# annee mois jour jour_de_la_semaine heure minute seconde subseconde(compteur interne)
#       year  m  j  wd h  m  s  sub
date = [2021, 1, 1, 5, 0, 0, 0, 0]
#indices : 0  1  2  3  4  5  6  7

# on initialise la date
rtc.datetime(date)

while True :
    # on récupère la date mise a jour
    date = rtc.datetime()
    # et on l'affiche
    print('{0:02d}'.format(date[4])+'h'+'{0:02d}'.format(date[5])+'min'+'{0:02d}'.format(date[6])+'s')
    #  on actualise toute les secondes
    time.sleep(1)
```  
On remarquera l'utilisation de `'{0:02d}'.format(date[4])` qui permet l'affichage de _02_ plutôt que _2_.  

En se servant du [tutoriel de l'afficheur 8x7-segments TM1638](./tuto_afficheur_8x7s.html) on peut réaliser une horloge avec affichage LED avec le code d'exemple suivant :  
```python
import tm1638
from machine import Pin
import time

# on declare la carte de l'afficheur
tm = tm1638.TM1638(stb=Pin('D2'), clk=Pin('D3'), dio=Pin('D4'))

# on declare la RTC (Real Time Clock)
rtc = pyb.RTC()

# on réduit la luminosité
tm.brightness(0)

# annee mois jour jour_de_la_semaine heure minute seconde subseconde(compteur interne)
#       year  m  j  wd h  m  s  sub
date = [2021, 1, 1, 5, 0, 0, 0, 0]
#indices : 0  1  2  3  4  5  6  7

# on initialise la date
rtc.datetime(date)

while True :
    # on récupère la date mise a jour
    date = list(rtc.datetime()) # rtc.datetime() renvoie un objet non modifiable, on le change en liste pour le modifier

    # et on l'affiche en faisant clignoter le point des secondes
    if (date[6] % 2) :
        tm.show('{0:02d}'.format(date[4])+'h'+'{0:02d}'.format(date[5])+' .'+'{0:02d}'.format(date[6]))
    else :
        tm.show('{0:02d}'.format(date[4])+'h'+'{0:02d}'.format(date[5])+' '+'{0:02d}'.format(date[6]))

    # on récupère l'information sur les boutons pour régler l'heure
    boutons = tm.keys()

    # on compare bit à bit pour identifier le bouton
    if (boutons & 1) :  
        date[4] += 1
        if (date[4] > 23) :
            date[4] = 0
        rtc.datetime(date)
    if (boutons & 1<<1) :
        date[5] += 1
        if (date[5] > 59) :
            date[5] = 0
        rtc.datetime(date)
    if (boutons & 1<<2) :
        date[6] += 1
        if (date[6] > 59) :
            date[6] = 0
        rtc.datetime(date)

    #  on actualise toute les 100 milisecondes (pour un lecture des boutons plus sensible, peut être modifié)
    time.sleep_ms(100)
```
