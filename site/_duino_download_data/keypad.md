---
layout: article
title: Clavier matriciel
description: Mise en oeuvre de la lecture d'une touche avec un clavier matriciel
permalink: keypad.html
key: page-aside
cover: /site/assets/cover/home_page/home.jpg
aside:
  toc: true
sidebar:
  nav: site_nav
---

Ce tutoriel explique comment récupérer la touche appuyée sur un calvier matriciel avec MicroPython.

<h2>Description</h2>

Un clavier matriciel, également appelé keypad en anglais, est un clavier permettant de récupérer des caractères (chiffres 0 à 9, lettres A à D, symboles * et #).

Ces touches sont disposées en matrice de 4x4 ou 3x4. Ces touches sont de simples boutons poussoirs.
Le dispositif est passif et comprend 7 ou 8 pattes physiques :
- 4 pattes reliées aux touches d'une même colonne
- 3 ou 4 pattes reliées aux touches d'une même ligne

<div align="center">
<img alt="Fonctionnement keypad" src="site/assets/images/stm32duino/servomoteur-pwm.svg/keypad-decomposition.png" width="80%">
</div>

Le clavier doit être utilisé comme indiqué comme ci-dessous :
1. Quatre pattes de la carte doivent être configurées comme sorties et quatres autres comme entrées. Des résistances de tirage peuvent être ajoutées afin de fixer l'état logique quand aucune touche n'est pressée.
2. Les pattes en sorties sont mises à l'état haut et on lit l'état des pattes en entrée. En pressant une touche cela fera apparaître un état logique haut sur une des pattes d'entrée.
3. En combinant les 0 et les 1 sur les pattes en sortie on peut déterminer quel bouton est pressé.


<h2>Montage</h2>

**Montage 1 :** Pour un clavier 3x4 on vient réaliser le montage suivant :

<div align="center">
<img alt="Schéma de montage 1 keypad" src="site/assets/images/stm32duino/servomoteur-pwm.svg/keypad-schema1.png" width="70%">
</div>

Lors du cablage faites attention à bien laisser une patte à chaque extremum de la connectique pour ce clavier ci.

**Montage 2 :** Pour un clavier 4x4 on vient réaliser le montage suivant :

<div align="center">
<img alt="Schéma de montage 2 keypad" src="site/assets/images/stm32duino/servomoteur-pwm.svg/keypad-schema2.png" width="70%">
</div>

Comme expliqué précédemment le dispositif est passif et n'a donc pas besoin d'alimentation.
On peut également rajouter des résistances de tirages de 1kOhms.


<h2>Programme</h2>

Afin de simplifier le code et le rendre plus propre nos avons créé une bibliothèque externe disponible [ici](https://gitlab.com/stm32python/fr/-/blob/master/assets/Script/Keypad/keypad.py). Téléchargez la et déposez la dans la carte Nucleo. Cette bibliothèque nous permet d'éviter de programmer les étapes décrites plus haut. Cependant vous pouvez toujours l'ouvrir et observer le comportement du code.

**Etape 1 :** Pour faire fonctionner le programme nous devons importer la bibliothèque téléchargée précédemment.
Dans le cas du **montage 1** avec un clavier 3x4 :
```python
from keypad import Keypad3x4
```

Dans le cas du **montage 2** avec un clavier 4x4 :
```python
from keypad import Keypad3x4
```

**Etape 2 :** On vient crée l'entité du clavier.
Dans le cas du **montage 1** avec un clavier 4x4 :
```python
clavier = Keypad3x4()
```

Dans le cas du **montage 2** avec un clavier 4x4 :
```python
clavier = Keypad4x4()
```

**Etape 3 :** Enfin on vient faire la lecture de la touche et on vient l'afficher dans le terminal série.
```python
while True:
	touche = clavier.read_key()
	print(touche)
```


<h2>Résultat</h2>

Il ne vous reste plus qu'à sauvegarder l'ensemble puis à redémarrer votre carte NUCLEO-WB55. Essayez les différentes touches, celles-ci devraient s'afficher dans le moniteur série.

En cas de problème verifiez votre câblage et/ou votre code pour voir si vous utilisez bien le bon type de clavier.

> Crédit image : [Mbed](https://os.mbed.com/components/Keypad-12-Button-COM-08653-ROHS/)
