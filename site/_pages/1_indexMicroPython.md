---
layout: article
title: STM32python
description: microPython et STM32WB
permalink: /stm32.html
key: page-single
cover: site/assets/cover/home_page/micropython.png

aside:
  toc: true
sidebar:
  nav: site_nav
---
<p>
<img align="center" src="site/assets/images/microPython/upython.png" alt="upython" width="100"/>
<img align="center" src="site/assets/images/microPython/st2.png" alt="stm" width="250"/>
</p>

Vous trouverez dans cette partie tous les tutoriels STM32python.

Avant toute chose, assurez vous d'avoir une installation fonctionnelle.
Le protocole à suivre est détaillé dans les 2 guides de démarrage.




<div class="layout--articles">
	<section class="my-5">
		<header><h2>Installation : </h2></header>
	  {%- include article-list.html articles=site.microPython_install type='grid' -%}
	</section>
</div>

<div class="layout--articles">
	<section class="my-5">
		<header><h2>Tutoriels </h2></header>
		<div class="layout--articles">
			<section class="my-5">
				<header><h3>Basic </h3></header>
					{%- include article-list.html articles=site.microPython_basic type='grid' -%}
			</section>
			<section class="my-5">
				<header><h3>Afficheurs </h3></header>
					{%- include article-list.html articles=site.microPython_tuto type='grid' -%}
			</section>
			<section class="my-5">
				<header><h3>Capteurs </h3></header>
					{%- include article-list.html articles=site.microPython_actuator type='grid' -%}
			</section>
			<section class="my-5">
				<header><h3>Autres </h3></header>
					{%- include article-list.html articles=site.microPython_advance type='grid' -%}
			</section>
		</div>
	</section>
</div>
<div class="layout--articles">
	<section class="my-5">
		<header><h2>About: </h2></header>
	  {%- include article-list.html articles=site.microPython_about type='grid' -%}
	</section>
</div>
