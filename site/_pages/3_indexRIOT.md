---
layout: article
title: RIOT
description: Comment utiliser le kit STM32python au moyen de RIOT OS
permalink: /riot.html
key: page-single
cover: site/assets/cover/home_page/Riot.jpg
aside:
  toc: true
sidebar:
  nav: site_nav
---

[RIOT OS](http://riot-os.org/) est un système d'exploitation temps-réel pour les micro-contrôleurs constraints en mémoire et en énergie. RIOT OS supporte un grand nombre de cartes STM32 Nucleo et Discovery. Les programmes peuvent être développés en C, Lua, Javascript et MicroPython.

## Sommaire

Vous trouverez dans cette partie des exercices pour programmer en MicroPython avec RIOT OS sur le kit pédagogique STM32python.

Avant toute chose, assurez vous d'avoir une installation fonctionnelle.

Le protocole à suivre est dans la section **installation**.

<div class="layout--articles">
  <section class="my-5">
    <header><h2 id="page-layout">Installation</h2></header>
    {%- include article-list.html articles=site.riot_install type='grid' -%}
  </section>
</div>

<div class="layout--articles">
  <section class="my-5">
    <header><h2 id="page-layout">Tutoriels</h2></header>
    {%- include article-list.html articles=site.riot_tuto type='grid' -%}
  </section>
</div>
