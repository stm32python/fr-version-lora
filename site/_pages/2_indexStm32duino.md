---
layout: article
title: STM32duino
description: Section STM32duino
permalink: /stm32duino.html
key: page-aside
cover: site/assets/cover/home_page/STM32duino.jpg
aside:
  toc: true
sidebar:
  nav: site_nav

---

[STM32duino](https://github.com/stm32duino) est un projet rassemblant des bibliothèques Arduino pour les cartes de développement STM32 (Nucleo et Discovery) et pour les composants MEMS de STMicroelectronics. Il permet de développer et compiler des programmes (ie. _sketch_) depuis l'environnement de développement Arduino IDE.

# Sommaire

Vous trouverez dans cette partie tous les exercices [STM32duino](https://github.com/stm32duino) pour le kit pédagogique STM32python.

Avant toute chose, assurez vous d'avoir une installation fonctionnelle.
Le protocole à suivre pour ce faire est détaillé dans la section **installation**.

## Installation
<div class="layout--articles">
	<section class="my-5">
	  {%- include article-list.html articles=site.duino_install type='grid' -%}
	</section>
</div>

## Tutoriels
<div class="layout--articles">
	<section class="my-5">
		<div class="layout--articles">
			<section class="my-5">
				<header><h3>Afficheurs </h3></header>
					{%- include article-list.html articles=site.duino_layers type='grid' -%}
			</section>
		</div>
		<div class="layout--articles">
			<section class="my-5">
				<header><h3>Capteurs </h3></header>
					{%- include article-list.html articles=site.duino_sensors type='grid' -%}
			</section>
		</div>
		<div class="layout--articles">
			<section class="my-5">
				<header><h3>Enregistrement de données </h3></header>
					{%- include article-list.html articles=site.duino_download_data type='grid' -%}
			</section>
		</div>
		<div class="layout--articles">
			<section class="my-5">
				<header><h3>Actuateurs </h3></header>
					{%- include article-list.html articles=site.duino_actuators type='grid' -%}
			</section>
		</div>
		<div class="layout--articles">
			<section class="my-5">
				<header><h3>Autre </h3></header>
					{%- include article-list.html articles=site.duino_other type='grid' -%}
			</section>
		</div>
	</section>
</div>
