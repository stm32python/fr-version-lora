---
layout: article
title: Fritzing
description: Comment utiliser Fritzing pour décrire des montages pour le kit STM32 MicroPython ?
permalink: /fritzing.html
key: page-single
cover: site/assets/cover/home_page/Fritzing.jpg

aside:
  toc: true
sidebar:
  nav: site_nav
---

[Fritzing](https://fritzing.org/) est une application qui permet de décrire des montages électroniques de manière pédagogique. Une importante bibliothèque de composants et des cartes (aka _parts_) est disponible.

## Sommaire
Vous trouverez dans cette partie un tutoriel sur l’utilisation de [Fritzing](https://fritzing.org/) avec le kit STM32 MicroPython.

## Installation de Fritzing
A rédiger

## Installation de la bibliothèque du _part_ STM32 Nucleo WB55 dans Fritzing
A rédiger

![Top](images/stm32nucleowb55-top.svg)
![Bottom](images/stm32nucleowb55-bottom.svg)

## Dessin d'un montage simple avec le carte STM32 Nucleo WB55
A rédiger

Installez les _parts_ Grove à partir du [dépôt Github](https://github.com/Seeed-Studio/fritzing_parts).

## Dessin d'un montage simple avec le carte STM32 Nucleo WL55JC
A rédiger

## Dessin d'un montage simple avec le composant Seeedstudio LoRa E5 Dev
A rédiger

## Dessin d'un montage simple avec le composant Seeedstudio LoRa E5 Mini
A rédiger

## Références
* [https://gitlab.com/stm32python/frizting](https://gitlab.com/stm32python/frizting)
