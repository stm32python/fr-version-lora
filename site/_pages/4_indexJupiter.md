---
layout: article
title: Jupyter
description: Comment utiliser le kit STM32python avec Jupyter
permalink: /jupyter.html
key: page-single
cover: site/assets/cover/home_page/Jupyter.jpg

aside:
  toc: true
sidebar:
  nav: site_nav
---

[Jupyter](https://jupyter.org/) est une application qui permet de programmer dans plusieurs langages de programmation comme Python, Julia, Ruby, R, ou encore Scala. Jupyter permet de réaliser des calepins ou notebooks mélant des notes au format Markdown, des programmes ainsi que les résultats de leurs exécutions.

## Sommaire

Vous trouverez dans cette partie le tutoriel vous permettant de créer un Notebook [Jupyter](https://jupyter.org/) pour traiter et afficher en temps réel les mesures relevées par les capteurs branchés sur la carte STM32.

<div class="layout--articles">
  <section class="my-5">
    <header><h2 id="page-layout">Installation</h2></header>
    {%- include article-list.html articles=site.jupyter_install type='grid' -%}
  </section>
</div>

<div class="layout--articles">
  <section class="my-5">
    <header><h2 id="page-layout">Tutoriels</h2></header>
    {%- include article-list.html articles=site.jupyter_tuto type='grid' -%}
  </section>
</div>
