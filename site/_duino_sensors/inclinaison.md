---
layout: article
title: Capteur d'inclinaison
description: Mise en oeuvre d'un capteur d'inclinaison Grove avec MicroPython
permalink: inclinaison.html
key: page-aside
cover: site/assets/cover/duino/tiltsensorim.png
aside:
  toc: true
sidebar:
  nav: site_nav
---

# Capteur d'inclinaison

Ce tutoriel explique comment mettre en oeuvre un capteur d'inclinaison Grove avec MicroPython.

**Prérequis :**

1. Une carte d'extension de base Grove
2. La carte NUCLEO-WB55
3. Un capteur d'inclinaison Grove

**Le capteur d'inclinaison Grove :**


<div align="center">
<img alt="Grove tilt sensor" src="site/assets/cover/duino/tiltsensorim.png" width="200px">
</div>

Le *capteur d’inclinaison* (*tilt-sensor* en anglais), change d’état lorsque son inclinaison par rapport à l'horizontale dépasse une valeur limite donnée. Il est constitué d'un tube cylindrique contenant une  bille métallique. Lorsqu'il est incliné, la bille suit son mouvement, roule, et vient faire contact à l'une des extrémités du tube).
Ce capteur peut donc être sous deux états (nommés "ON" et "OFF" dans le code qui suit).
Branchez le capteur sur le **connecteur D4** du Grove Base Shield.


**Le code MicroPython :**

```python
# Objet : mise en oeuvre d'un interrupteur à bille / capteur d'inclinaison

import pyb
import time # Bibliothèque de temporisation
from pyb import Pin

p_in = Pin('D4', Pin.IN, Pin.PULL_UP)

while True :

	time.sleep_ms(500) # Temporisation de 500 millisecondes

	etat = p_in.value() # Lecture du capteur, 0 si horizontal et 1 si incliné

	if etat:
		print("ON")
	else:
		print("OFF")
```

> Crédit image : [Seeedstudio](http://wiki.seeedstudio.com/Grove_System/)
