---
layout: article
title: Sonde étanche de température DS1820
description: Mise en oeuvre la sonde étanche DS1820 Grove en MicroPython
permalink: ds1820.html
key: page-aside
cover: site/assets/cover/duino/grove-ds1820.jpg
aside:
  toc: true
sidebar:
  nav: site_nav
---

# Sonde étanche de température DS1820

Ce tutoriel explique comment mettre en oeuvre la sonde étanche DS1820 Grove en MicroPython. Celle-ci utilise le protocole de communication One-Wire.
One-Wire, également connu sous le nom de bus Dallas ou 1-Wire, est un bus conçu par Dallas Semiconductor qui permet de connecter (en série, parallèle ou en étoile) des composants avec seulement deux fils (un fil de données et un fil de masse).

**Matériel requis :**

1. Une carte d'extension de base Grove
2. La carte NUCLEO-WB55
3. Un capteur de température Grove OneWire DS1820

**Le capteur de température Grove OneWire DS1820 :**

<div align="center">
<img alt="Grove - DS1820" src="site/assets/cover/duino/grove-ds1820.jpg" width="400px">
</div>

Le capteur est à brancher sur la broche A0 du Grove base shield.

**Le code MicroPython :**

Les librairies nécessaires au bon fonctionnement du capteur de température One-Wire DS18B20 sont disponibles dans la [zone de téléchargement](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement)<br>
Il faut récupérer et ajouter les fichiers **ds18x20.py** et **onewire.py** ([sources](https://github.com/micropython/micropython)) dans le répertoire du périphérique PYBLASH.

Editez maintenant le script *main.py* :

```python
# Source : https://github.com/micropython/micropython
# Objet du script : Mesure de température toutes les secondes.
# Affiche les valeurs mesurées sur le port série de l'USB user
# Cet exemple nécessite un Grove Base Shield et un capteur de température DS18X20 connecté sur A0

import time
import machine
import onewire #Bibliothèque pour le protocole OneWire
import ds18x20 #Bibliothèque pour le capteur de température DS18X20

# On a connecté notre capteur à la broche A0
dat = machine.Pin('A0')

# Crée un objet "OneWire" (instance du protocole OneWire)
onew = onewire.OneWire(dat)

# Connecte la sonde à cette instance
ds = ds18x20.DS18X20(onew)

# Recherche des périphériques sur le bus OneWire
roms = ds.scan()
print('Périphériques trouvés:', roms)

# Effectue 20 mesures de température consécutives
for i in range(20):
	print('Température (degrés Celsius) :', end=' ')
	ds.convert_temp()
	time.sleep_ms(1000)
	# Pour tous les objetcs connectés au OneWire
	for rom in roms:
		temp = round(ds.read_temp(rom),1)
		print(temp, end=' ')
	# Imprime une ligne blanche
	print()
```
Le code consiste à lire 20 fois une température et afficher ces mesures sur un terminal série.

**Affichage sur le terminal série de l'USB User :**

<div align="center">
<img alt="One Wire output" src="site/assets/images/stm32duino/onewire_output.png" width="800px">
</div>

> Crédit image : [Seeedstudio](http://wiki.seeedstudio.com/Grove_System/)
