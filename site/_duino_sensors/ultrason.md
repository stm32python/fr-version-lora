---
layout: article
title: Capteur de distance à ultrason Grove en C/C++
description: Exercice avec le capteur de distance à ultrason Grove en C/C++ pour Stm32duino
permalink: capteur_ultrason.html
key: page-aside
cover: site/assets/cover/duino/capteur_ultrason.png
aside:
  toc: true
sidebar:
  nav: site_nav
---
**- Prérequis :**


**- Le capteur d'ultrasons (Ultrasonic Ranger):**

Ce capteur permet de mesure des distances précisement via des ultrasons. Un transducteur à ultrasons émet des ultrasons qui rebondissent ensuite sur les parois, un deuxième transducteur récepteur reçoit  ces ondes. Il est alors possible de calculer la distances parcourue par les ultrasons. On peut mesurer des distances de quelques centimètres à un ou deux mètres.
Ce capteur est semblable aux capteurs de recul des voitures.


![Image](site/assets/cover/duino/capteur_ultrason.png)

 *T --> transducteur émetteur (transmetteur)*  |  *R --> transducteur récepteur*

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
