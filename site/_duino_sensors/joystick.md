---
layout: article
title: Joystick Grove en C/C++
description: Exercice avec le joystick Grove en C/C++ pour Stm32duino
permalink: joystick.html
key: page-aside
cover: site/assets/cover/duino/joystick.png
aside:
  toc: true
sidebar:
  nav: site_nav
---
- **Prérequis :**

Cf tuto du tilt-sensor. Cependant il faut brancher le joystick sur le pin A0.

- **Le thumb-joystick :**


![Image]( site/assets/cover/duino/joystick.png)


Ce joystick est similaire à ceux que l'on peut retrouver sur une manette de PS2. Chacun des axes est relié à un potentiomètre  qui va fournir une tension correspondante. De plus le joystick possède un bouton poussoir.

*Voici le code sur Arduino*
```c
void setup() {
  Serial.begin(9600);
  pinMode(A0,INPUT);                //en branchant le joystick sur A0,l'axe Y sera lu par A0 et l'axe X par A1
  pinMode(A1,INPUT);                //sur A1, Y sera lu par A1 et X par A2; sur A2, Y sera lu par A2 et X par A3...
}

void loop() {

  int X=analogRead(A0);
  int Y=analogRead(A1);
  if(X<=780 && X>=750)
  {
    Serial.println("Haut");
  }
  if(X<=280 && X>=240)
  {
    Serial.println("Bas");
  }
  if(Y<=780 && Y>=750)
  {
    Serial.println("Gauche");
  }
  if(Y<=280 && Y>=240)
  {
    Serial.println("Droite");
  }
  if(X>=1000)                     //en appuyant sur le joystick, la sortie de l'axe X se met à 1024, le maximum.
  {
    Serial.println("Appuyé");
  }
  delay(1000);

}
```

On peut alors vérifier nos input dans le moniteur série.

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
