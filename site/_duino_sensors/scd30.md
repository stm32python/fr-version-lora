---
layout: article
title: Capteur de CO2 SCD30
description: Mise en oeuvre du capteur de CO2 SCD30 Grove avec MicroPython
permalink: scd30.html
key: page-aside
cover: site/assets/cover/duino/grove_scd30.jpg
aside:
  toc: true
sidebar:
  nav: site_nav
---

Ce tutoriel explique comment mettre en oeuvre le capteur de CO2 I2C SCD30 Grove avec MicroPython.

**Matériel requis :**

1. Une carte d'extension de base Grove
2. La carte NUCLEO-WB55
3. Un module SCD30 de Grove

**Le module Grove SCD30 :**

<div align="center">
<img alt="Grove - SCD30" src="site/assets/cover/duino/grove_scd30.jpg" width="500px">
</div>

**Objectif de l'exercice :**

Réaliser des mesures de concentration en dioxyde de carbone (CO2, par spectrométrie infrarouge), température et humidité. Le capteur doit être connecté sur le connecteur I2C du Grove Base Shield.

**Le code MicroPython :**

Ce code est adapté de [l'exemple fourni ici](https://pypi.org/project/micropython-scd30/).

Les fichiers nécessaires au bon fonctionnement du module sont disponibles dans la [zone de téléchargement](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement). Il faut ajouter le fichier *scd30.py* dans le répertoire du périphérique PYBLASH.<br>

Editez maintenant le script *main.py* :

```python
# Source : https://pypi.org/project/micropython-scd30/
# Objet du script : Mise en oeuvre du module I2C Grove SCD30.

import time
from machine import I2C, Pin
from scd30 import SCD30

# On utilise l'I2C n°1 de la carte NUCLEO-W55 pour communiquer avec le capteur
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
time.sleep_ms(1000)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c.scan()))

# Instanciation du capteur
scd30 = SCD30(i2c, 0x61)

# Corrige la pression grâce à l'altitude du lieu (en m)
altitude = 0

while True:

	# Attend que le capteur renvoie des valeurs (par défaut toutes les 2 secondes)
	while scd30.get_status_ready() != 1:
		time.sleep_ms(200)

	# Lecture des valeurs mesurées
	scd30data = scd30.read_measurement()

	# Séparation et formattage (arrondis) des mesures
	conco2 = int(scd30data[0])
	temp = round(scd30data[1],1)
	humi = int(scd30data[2])

	# Affichage des mesures
	print('=' * 40) # Imprime une ligne de séparation
	print("Concentration en CO2 : " + str(conco2) + " ppm")
	print("Température : " + str(temp) + " °C")
	print("Humidité relative : " + str(humi) + " %")
```

**Sortie sur le port série de l'USB USER :**

<div align="center">
<img alt="Grove - SCD30 sortie" src="site/assets/images/stm32duino/grove_scd30_output.png" width="450px">
</div>

> Crédit image : [Seeedstudio](http://wiki.seeedstudio.com/Grove_System/)
