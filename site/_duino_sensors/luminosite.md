---
layout: article
title: Capteur de luminosité Grove en C/C++
description: Exercice avec le capteur de luminosité Grove en C/C++ pour Stm32duino
permalink: capteur_luminosite.html
key: page-aside
cover: site/assets/cover/duino/capteur-de-lumiere.png
aside:
  toc: true
sidebar:
  nav: site_nav
---

**Cf tuto capteur tiltsensor pour les prérequis**


- **Capteur de luminosité :**

![Image](site/assets/cover/duino/capteur-de-lumiere.png)

Ce capteur utilise une photorésistance afin de détecter l'intensité lumineuse de son environnement. La valeur de cette résistance diminue lorsque qu'elle est éclairée.


- *Voici le code sur Arduino*

```c
void setup() {
  Serial.begin(9600);
  pinMode(D4,INPUT);

}
void loop() {
  Serial.println(digitalRead(D4));
  delay(500);
}
```

Vérifiez et téléversez.  
Pour regarder l'état dans lequel se trouve le capteur cliquez sur le *moniteur série*.

![Image](site/assets/images/stm32duino/moniteur_serie.png)

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
