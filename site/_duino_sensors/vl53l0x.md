---
layout: article
title: Télémètre infrarouge VL53L0X
description: Mise en oeuvre du télémètre infrarouge VL53L0X Grove en MicroPython
permalink: capteur_vl53l0x.html
key: page-aside
cover: site/assets/cover/duino/grove-vl53l0x.jpg
aside:
  toc: true
sidebar:
  nav: site_nav
---

Ce tutoriel explique comment mettre en oeuvre Télémètre infrarouge VL53L0X en MicroPython. Celui-ci utilise le protocole de communication I2C.<br>
Le VL53L0X mesure la distance selon le même principe qu'un sonar ou un radar *actif*. Il émet une onde qui se propage devant lui jusqu'à ce qu'elle rencontre un obstacle qui la réfléchit partiellement. Lorsque l'onde réfléchie atteint le capteur, celui-ci la détecte et mesure le temps de son aller-retour. Connaissant la vitesse de propagation de l'onde, le VL53L0X calcule la distance à l'obstacle qui l'a renvoyée vers lui.
Dans le cas d'un sonar l'onde utilisée est une *onde sonore* (des vibrations de l'air). Dans le cas du VL53L0X qui nous intéresse, c'est une *onde électromagnétique cohérente* (de la lumière laser) de fréquence (couleur) infrarouge.<br>
La mesure de la distance est plus rapide et plus précise qu'avec un sonar. La portée maximum du VL53L0X est de deux mètres.

**Matériel requis :**

1. Une carte d'extension de base Grove
2. La carte NUCLEO-WB55
3. Un télémètre infrarouge VL53L0X Grove (Time of Flight distance sensor)
4. Un module LED Grove.

**Le télémètre infrarouge VL53L0X Grove :**

<div align="center">
<img alt="Grove - VL53L0X" src="site/assets/cover/duino/grove-vl53l0x.jpg" width="400px">
</div>

*Attention* le capteur de distance est situé au *verso* du module Grove, sur la face opposée à celle du connecteur !

**Le code MicroPython :**

Ce code a été adapté à partir [de ce site](https://github.com/uceeatz/VL53L0X).
Il faut récupérer le fichier **VL53L0X.py** (disponible sur la page [téléchargements](Telechargement)) et le copier dans le répertoire du périphérique PYBLASH.
Le module télémètre devra être branché sur un connecteur I2C du Grove Base Shield et le module LED sur sa prise D6.

Le programme réalise les fonctions suivantes :
 - Mise en oeuvre d'une PWM pour réaliser un [variateur de lumière](variateur_lumiere).
 - Contrôle de l'intensité lumineuse au moyen d'un capteur de distance VL53L0x.

L'intensité de la LED varie en fonction de la distance mesurée par le capteur (plus la distance est courte, plus la lumière est intense). Pour rendre l'application plus réaliste (et plus bruyante) vous pouvez remplacer le module LED par un module buzzer de Grove, et vous fabriquerez ainsi un mini radar de recul.

Editez maintenant le script *main.py* :

```python
from pyb import Pin, Timer
import time

# initialisation de la PWM
p = Pin('D6')
ti = 1 # Timer
ch = 1 # Canal
tim = Timer(ti, freq=1000) # Fréquence de la PWM fixée à 1kHz
ch = tim.channel(ch, Timer.PWM, pin=p)
i = 0

# initialisation du capteur VL53L0x
from machine import I2C
import VL53L0X # Bibliothèque pour le VL53L0X

#Initialisation du périphérique I2C
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
time.sleep_ms(1000)

tof = VL53L0X.VL53L0X(i2c) # Instance du capteur

while True:

	tof.start() # démarrage du capteur

	distance = tof.read() # mesure de distance

	tof.stop() # arrêt du capteur

	print( str(distance) + ' mm')

	# Convertit la distance en un nombre entre 0 et 100
	rapport_cyclique = 100 - min( distance // 10 , 100)

	# Applique ce rapport cyclique à la PWM de la LED
	ch.pulse_width_percent(rapport_cyclique)

	# Pause de 5 millisecondes
	time.sleep_ms(5)
```

> Crédit image : [Seeedstudio](http://wiki.seeedstudio.com/Grove_System/)
