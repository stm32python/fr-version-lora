---
layout: article
title: Servomoteur
description: Mettre en oeuvre un servomoteur avec MicroPython
permalink: servomoteur.html
key: page-aside
cover: /site/assets/cover/home_page/home.jpg
aside:
  toc: true
sidebar:
  nav: site_nav
---
Ce tutoriel explique comment mettre en oeuvre un servomoteur avec MicroPython.

<h2>Description</h2>
Le servomoteur (ou *servo*) est un type de moteur électrique. C'est un boitier contenant :

- une partie mécanique avec un moteur (souvent très petit) et des engrenages pour avoir une vitesse plus faible mais un couple plus important.
- une partie électronique avec un capteur agissant comme un potentiomètre dont la résistance varie en fonction de l'angle.

On le retrouve dans différents domaines, notamment industriel, pour ouvrir ou fermer des vannes mais aussi dans le modélisme, pour des voitures télécommandées par exemple.

Un servomoteur est donc un moteur dont on contrôle l'angle de rotation, qui peut varier de 0 à 180°, en fonction du signal envoyé par la carte Nucleo. Ce signal est de type [PWM](https://stm32python.gitlab.io/site/docs/Micropython/glossaire).

**Qu'est-ce que la PWM ?**

La _modulation en largeur d'impulsion (PWM pour "Pulse Width Modulation")_ est une méthode qui permet de simuler un signal analogique en utilisant une source numérique.
On génère un signal carré, de fréquence donnée, qui est à +3.3V (haut) pendant une proportion paramétrable de la période et à 0V (bas) le reste du temps. C’est un moyen de moduler l’énergie envoyée à un actuateur. Usages : commander des moteurs, faire varier l’intensité d’une LED, faire varier la fréquence d’un buzzer... La proportion de la durée d'une période pendant laquelle la tension est dans l'état "haut" est appelée _rapport cyclique_.

Dans notre cas la PWM est un signal périodique de 50Hz (soit 20ms) dont on fait varier le rapport cyclique entre 5% (1 ms / 20 ms) et 10% (2 ms / 20 ms) pour faire tourner le bras du servo de 0° à 180°, conformément aux schémas qui suivent :

<div align="center">
<img alt="Description PWM servomoteur" src="site/assets/images/stm32duino/servomoteur-pwm.svg" width="400px">
</div>

**Programmer des PWM sur la carte NUCLEO-WB55**

En pratique, la génération de signaux PWM est l’une des fonctions assurées par les _timers_, des composants intégrés dans le microcontôleur qui se comportent comme des compteurs programmables. Le microcontrôleur de notre carte contient plusieurs timers, et chacun d'entre eux pilote plusieurs _canaux_ (_channels_ en anglais). Certains canaux de certains timers sont finalement connectés à des broches de sorties du Microcontôleur (GPIO). Ce sont ces broches là qui vont pouvoir être utilisées pour générer des signaux de commande PWM.

Même si MicroPython permet de programmer facilement des broches en mode sortie PWM, vous aurez cependant besoin de connaitre :
 1. Quelles broches de la NUCLEO-WB55 sont des sorties PWM ;
 2. A quels timers et canaux sont connectées ces broches dans le STM32WB55.

La figure ci-dessous répond à ces deux questions :

<div align="center">
<img alt="PWM WB55" src="site/assets/images/stm32duino/pwm_wb55.jpg" width="400px">
</div>


<h2>Montage</h2>

Le servomoteur dispose d'une connectique avec des couleurs qui lui sont propres (mais qui sont standardisées). Il faut cependant bien vérifier la correspondance des couleurs en fonction des connecteurs. Dans notre cas nous venons les connecter de cette façon sur la carte :

<div align="center">
<img alt="Schéma de montage servomoteur" src="images/servomoteur-schema.png" width="800px">
</div>

La correspondance entre le servomoteur et la carte NUCLEO-WB55 est la suivante :

| Servomoteur       | Couleur du fil    | ST Nucleo         |
| :-------------:   | :---------------: | :---------------: |
|       Signal      |      Orange       |         D6        |
|       GND         |      Marron       |         GND       |
|       5V          |      Rouge        |         5V        |


<h2>Programme</h2>

**Etape 1 :** Pour faire fonctionner le programme nous devons dans un premier temps importer 2 bibliothèques au tout début de notre code de cette façon :
```python
import pyb
import time
```

**Etape 2 :** Ensuite il faut venier initialiser la patte sur laquelle est connecté le servomoteur. On vient également configurer la PWM avec une fréquence de 50Hz.
```python
servo = pyb.Pin('D6')
tim_servo = pyb.Timer(1, freq=50)
```

**Etape 3 :** Il ne nous reste plus qu'à faire tourner le servo. Pour cela on vient créer une boucle infinie qui fait pivoter le servo de 90° toutes les deux secondes.
```python
while True:
	tim_servo.channel(1, pyb.Timer.PWM, pin=servo, pulse_width_percent=12.5)		#Servo à 90 degrés
	time.sleep(2)
	tim_servo.channel(1, pyb.Timer.PWM, pin=servo, pulse_width_percent=7.5)			#Servo à 0 degré
	time.sleep(2)
	tim_servo.channel(1, pyb.Timer.PWM, pin=servo, pulse_width_percent=3)			#Servo à -90 degrés
	time.sleep(2)
	tim_servo.channel(1, pyb.Timer.PWM, pin=servo, pulse_width_percent=7.5)			#Servo à 0 degré
	time.sleep(2)

tim_servo.deinit()																    #Arret du timer du servo
```

<h2>Résultat</h2>

Après avoir sauvegardé et redémarré votre carte Nucleo vous pouvez voir le servomoteur tourner de 90° toutes les deux secondes.
Pour aller plus loin vous pouvez également utiliser le joystick d'une manette pour contrôler le servomoteur comme expliqué [ici](https://stm32python.gitlab.io/fr/docs/Micropython/grove/nunchuk).

> Crédit image : [Wikimedia](https://upload.wikimedia.org/wikipedia/commons/f/f6/TiemposServo.svg)
