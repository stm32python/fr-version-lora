---
layout: article
title: Documentation
permalink: documentation.html
key: page-single
cover: site/assets/cover/home_page/Document.jpg
---

Bienvenue sur la page de documentation de {{ site.title }}!
Ici vous pouvez accéder directement aux différentes parties du sites.

<div class="section-index">
    <hr class="panel-line">
    {% for post in site.pages  %}        
    <div class="entry">
    <h5><a href="{{ post.url | prepend: site.baseurl }}">{{ post.title }}</a></h5>
    <p>{{ post.description }}</p>
    </div>{% endfor %}
</div>
