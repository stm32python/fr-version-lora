---
layout: article
title: Glossaire
description: Glossaire
permalink: glossaire.html
key: page-single
cover: site/assets/cover/home_page/Book.jpg
---

**GPIO** : General Purpose Input Output (fr : Port D’entrée Sortie).

**LED** : Luminescent Electronic Diode (fr: Diode ÉlectroLuminescente).

**ADC** : Analog to Digital Converter (fr : Convertisseur Analogique vers Numérique).

**DAC** : Digital to Analog Converter (fr : Convertisseur Numérique vers Analogique).

**TIM** : Timer (fr : Compteur).

**UART** : Universal Asynchronous Receiver Transmitter (fr : Récepteur Transmetteur Asynchrone Universel).

**I2C** : Inter-Integrated Circuit (fr : _bus de communication_ intégré inter circuit).

**SPI** : Serial Peripheral Interface (fr: Interface Série pour Périphériques).

**LCD** : Liquid Crystal Display (fr: afficheur à cristaux liquide).

**OLED** : Organic Luminescent Electronic Diode (fr: Diode ÉlectroLuminescente Organique) .

**µC** : Microcontrôleur.

**ALU** : Arithmetic and Logic Unit (fr : Unité d’opération arithmétique et logique)

**BLE** : Bluetooth Low Energy (fr : Bluetooth faible consommation énergétique).

**Cortex M4** : Type de coeur d’un microcontrôleur développé par la société ARM.

**RAM** : Type de mémoire volatile dans laquelle sont placées les variables.

**FLASH** : Type de mémoire non volatile dans laquelle est placé le code compilé.

**ST-LINK V2** : Programmeur ST pour transférer le code dans les mémoires FLASH et RAM d’un microcontrôleur.

**NUCLEO** : Nom commercial donné aux cartes de développement fabriquées par STMicroelectronics.

**Morpho** : Type de connecteur propriétaire ST sur les cartes NUCLEO, donnant accès à l'ensemble des broches du microcontrôleur.

**Arduino** : Écosystème éducatif de cartes électroniques développé par la société du même nom.

**Firmware** : Nom donné au fichier binaire qui contient un programme compilé destiné à un microcontrôleur.

**Bootloader** : Système logiciel de démarrage permettant la mise à jour du firmware.

**DFU** : Device Firmware Update(fr: Mise à jour firmware du système).
