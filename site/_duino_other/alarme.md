---
layout: article
title: Alarme de mouvement
description: Création d'une alarme de mouvement avec un buzzer et un capteur de mouvements PIR sous MicroPython
permalink: alarme.html
key: page-aside
cover: /site/assets/cover/home_page/home.jpg
aside :
  toc: true
sidebar:
  nav: site_nav
---

Dans ce tutoriel nous utiliserons en parallèle le [buzzer](buzzer) ainsi que le détecteur de mouvement [PIR motion sensor](mouvement).
Lorsqu'un mouvement sera détecté, le buzzer produira un son (alarme) pendant une seconde.

**Matériel requis :**

1. Une carte d'extension de base Grove
2. La carte NUCLEO-WB55
3. Un buzzer Grove
4. Un détecteur de mouvement PIR Grove

Brancher le buzzer sur le connecteur **D3** et le détecteur sur le connecteur **D2**.
Si un mouvement est détecté, l'interruption se déclenche ce qui provoque l'activation de la LED et du buzzer.

**Le code MicroPython :**

```python
# Objet du script : conception d'un système d'alarme.
# Un capteur de mouvement PIR est configuré en interruption sur la broche D2.
# Un buzzer est connecté sur la broche D3 et piloté par une PWM.
# Matériel requis :
#   - Un capteur PIR (de préférence fonctionnant en 3.3V)
#   - Un buzzer (de préférence fonctionnant en 3.3V)
# On utilise une interruption pour capturer le signal du détecteur de mouvements.

from machine import Pin
from time import sleep_ms
from pyb import Pin, Timer

# Configuration des LED
led_bleu = pyb.LED(1)
led_rouge = pyb.LED(3)

# Configuration du buzzer
frequency = 440
buz = Pin('D3')
# D3 génère une PWM avec TIM1, CH3
timer1 = Timer(1, freq=frequency)
channel3 = timer1.channel(3, Timer.PWM, pin=buz)

# Configuration du capteur PIR
PIR_Pin = Pin('D2', Pin.IN)
motion = False

# Fonction de gestion de l'interruption du capteur PIR
def handle_interrupt(pin):
	global motion
	motion = True
	global interrupt_pin
	interrupt_pin = PIR_Pin

# Activation de l'interruption du capteur PIR
PIR_Pin.irq(trigger=PIR_Pin.IRQ_RISING, handler=handle_interrupt)

# Boucle principale
while True:
	if motion:
		print('Mouvement détecté !')
		led_rouge.on()
		led_bleu.off()
		# Rapport cyclique paramétré à 5% (le buzzer est alimenté 5% du temps d'une période)
		channel3.pulse_width_percent(5)
		sleep_ms(1000) # Le buzzer sonne pendant une seconde
		print('Détecteur de mouvement activé')
		led_bleu.on()
		led_rouge.off()
		# Rapport cyclique paramétré à 0% (le buzzer n'est plus alimenté)
		channel3.pulse_width_percent(0)
		motion = False

```

**Affichage sur le terminal série de l'USB User :**

![Image](site/assets/images/stm32duino/alarm_output.png)
