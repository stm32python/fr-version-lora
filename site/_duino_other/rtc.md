---
layout: article
title: Horloge temps-réel
description: Mise en oeuvre de la Real Time Clock (RTC) du STM32WB55 en MicroPython
permalink: rtc.html
key: page-aside
cover: /site/assets/cover/home_page/home.jpg
aside:
  toc: true
sidebar:
  nav: site_nav
---

Ce tutoriel explique comment mettre en oeuvre l'horloge temps réel du STM32WB55 en MicroPython.

L'horloge temps réel (Real-Time Clock en anglais, abrégée RTC) est un circuit électronique intégré dans le microcontrôleur STM32WB55 remplissant les fonctions d’une horloge très précise pour des problématiques d’horodatage, de gestions d’alarmes déclenchées suivant un calendrier et capable de mettre le microcontrôleur en sommeil ou de le réveiller, etc. Afin qu’elle soit aussi précise et stable que possible, la RTC peut utiliser comme source de fréquence un quartz vibrant à 32kHz.

## Utilisation

La RTC est assez simple à utiliser puisque seulement une méthode nous servira : `pyb.RTC().datetime()`.
La date utilisée prendra alors le format suivant :
- \[year,  m,  j,  wd,  h,  m,  s,  sub]
- année  mois  jour  jour_de_la_semaine  heure  minute  seconde  subsecond(compteur interne)

Nous commencerons par donner un nom à la RTC
```python
rtc = pyb.RTC()
```
`rtc.datetime()` utilisée seule renvoie la date actuelle au format indiqué.
`rtc.datetime(date)` remplace la date actuelle.
La date du samedi 2 janvier 2021 à 12h21 51s peut alors être définie comme ceci :
```python
rtc.datetime((2021, 1, 2, 6, 12, 21, 51, 0))
```
et elle peut être récupérée comme cela :
```python
date = rtc.datetime()
```

## Exemples

Ainsi un premier code pour afficher sur le terminal serait :
```python
# Objet du script : Mise en oeuvre de l'horloge temps réel (RTC pour "Real Time Clock")
# du STM32WB55.

import time # Bibliothèque de temporisation

# on declare la RTC (Real Time Clock)
rtc = pyb.RTC()

# annee mois jour jour_de_la_semaine heure minute seconde subseconde(compteur interne)
#	   year  m  j  wd h  m  s  sub
date = [2021, 1, 1, 5, 0, 0, 0, 0]
#indices : 0  1  2  3  4  5  6  7

# on initialise la date
rtc.datetime(date)

while True :
	# on récupère la date mise a jour
	date = rtc.datetime()
	# et on l'affiche
	print('{0:02d}'.format(date[4])+'h'+'{0:02d}'.format(date[5])+'min'+'{0:02d}'.format(date[6])+'s')
	#  on actualise toute les secondes
	time.sleep(1)
```
On remarquera l'utilisation de `'{0:02d}'.format(date[4])` qui permet l'affichage de _02_ plutôt que _2_.

En se servant du [tutoriel de l'afficheur 8x7-segments TM1638](https://stm32python.gitlab.io/fr/docs/Micropython/grove/tm1638) on peut réaliser une horloge avec affichage LED avec le code d'exemple suivant :
```python
# Objet du script : Mise en oeuvre de l'horloge temps réel (RTC pour "Real Time Clock")
# du STM32WB55.
# Création d'une horloge LED avec un afficheur 8x7-segments TM1638.

import tm1638
from machine import Pin
import time

# on declare la carte de l'afficheur
tm = tm1638.TM1638(stb=Pin('D2'), clk=Pin('D3'), dio=Pin('D4'))

# on declare la RTC (Real Time Clock)
rtc = pyb.RTC()

# on réduit la luminosité
tm.brightness(0)

# annee mois jour jour_de_la_semaine heure minute seconde subseconde(compteur interne)
#	   year  m  j  wd h  m  s  sub
date = [2021, 1, 1, 5, 0, 0, 0, 0]
#indices : 0  1  2  3  4  5  6  7

# on initialise la date
rtc.datetime(date)

while True :
	# on récupère la date mise a jour
	date = list(rtc.datetime()) # rtc.datetime() renvoie un objet non modifiable, on le change en liste pour le modifier

	# et on l'affiche en faisant clignoter le point des secondes
	if (date[6] % 2) :
		tm.show('{0:02d}'.format(date[4])+'h'+'{0:02d}'.format(date[5])+' .'+'{0:02d}'.format(date[6]))
	else :
		tm.show('{0:02d}'.format(date[4])+'h'+'{0:02d}'.format(date[5])+' '+'{0:02d}'.format(date[6]))

	# on récupère l'information sur les boutons pour régler l'heure
	boutons = tm.keys()

	# on compare bit à bit pour identifier le bouton
	if (boutons & 1) :
		date[4] += 1
		if (date[4] > 23) :
			date[4] = 0
		rtc.datetime(date)
	if (boutons & 1<<1) :
		date[5] += 1
		if (date[5] > 59) :
			date[5] = 0
		rtc.datetime(date)
	if (boutons & 1<<2) :
		date[6] += 1
		if (date[6] > 59) :
			date[6] = 0
		rtc.datetime(date)

	#  on actualise toute les 100 milisecondes (pour un lecture des boutons plus sensible, peut être modifié)
	time.sleep_ms(100)
```
