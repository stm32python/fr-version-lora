---
layout: article
title: Windows 10
description: Guide de démarrage rapide sous Windows 10
permalink: installation_windows10.html
key : page - aside
cover: site/assets/cover/microPython/win10.jpg

aside:
  toc: true
sidebar:
  nav: site_nav

---

> Ces étapes sont à réaliser par le professeur ayant un accès administrateur sous Windows 10, pour chaque carte NUCLEO-WB55 utilisée en TP.

Dans ce guide de démarrage, nous allons:
- Programmer la carte avec le micrologiciel
- Utiliser un terminal de communication UART (TeraTerm ou PUTTY)
- Programmer sur la NUCLEO notre premier script Python

## Configuration électronique

**Vous aurez besoin d’un câble USB vers micro USB.**

Cette configuration se résume à :

- Assurez-vous que le cavalier est positionné sur ***USB_STL***
- Connectez un câble micro USB sur le port ***ST_LINK*** en ***dessous des LED4 et LED5***

Voici comment doit être configuré le kit :

![image](site/assets/images/microPython/Nucleo_WB55_Config_STL.png)

Ces modifications permettent de configurer le STM32WB55 en mode programmation STL_Link.<br>
Nous pourrons ainsi mettre à jour le firmware (fichier binaire) par USB_ST_Link.

## Programmation des cartes

Téléchargez et stockez le [Firmware](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement) (fichier .bin)

Reliez la carte NUCLEO-WB55(USB_STLINK) avec le câble USB à votre ordinateur.

### Installation du micrologiciel

Ouvrir le lecteur ***NOD_WB55***

![image](site/assets/images/microPython/Nod_wb55.PNG)

Faire un glisser-déposer du fichier [Firmware](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement) (fichier .bin).<br>
Le micrologiciel est maintenant intégré (il n'est plus visible dans le lecteur NOD_WB55)

Placez le cavalier sur ***USB_MCU*** ainsi que le câble micro USB sur le connecteur ***USB_USER*** sous le ***bouton RESET***

![image](site/assets/images/microPython/Nucleo_WB55_Config_MCU.png)

***Redémarrez la carte Nucleo***

Votre carte Nucleo est maintenant capable d'interpréter un fichier .py

Regardons les fichiers générés par le système MicroPython avec l'explorateur Windows :

Ouvrir le périphérique PYBFLASH avec l'explorateur Windows.

![Image](site/assets/images/microPython/dfu_util_3.png)

Nous verrons plus tard comment éditer les scripts Python disponibles dans le système de fichier PYBFLASH.

**Remarques importantes :**
Le script `boot.py` peut être modifié par les utilisateurs avancés, il permet d'initialiser MicroPython et notamment de choisir quel script sera exécuté après le démarrage de MicroPython, par défaut le script `main.py`.<br>

## Installation de l'environnement de programmation

Trois solutions s'offrent à vous:
- Utiliser la version portable ou installer (droits admin requis) [Notepad++](https://drive.google.com/drive/folders/1cvxYGfjHUo5_WrzCS3nMCThETnBiXGI0?usp=sharing) avec la coloration synthaxique Python et un terminal série ([PuTTY](https://drive.google.com/drive/folders/1QaN4n6_zORZ7P8XOjuydCYZOuSsGiICq?usp=sharing), [TeraTerm](https://drive.google.com/drive/folders/1ZHIoUInsrr_ekHqgWNX5gP6gFnjybX6M?usp=sharing)...)
- Installer (droits admin requis) [Thonny](https://drive.google.com/drive/folders/1iXiL6h0Fg4It9dGXlwMlYXABr6kiJjk5?usp=sharing) et son terminal integré
- Utiliser la version portable de [PyScripter](https://drive.google.com/drive/folders/1EI5MaYYN4TEkXPOmChPhXUfrTyALq4LH?usp=sharing) et un terminal série ([PuTTY](https://drive.google.com/drive/folders/1QaN4n6_zORZ7P8XOjuydCYZOuSsGiICq?usp=sharing), [TeraTerm](https://drive.google.com/drive/folders/1ZHIoUInsrr_ekHqgWNX5gP6gFnjybX6M?usp=sharing)...).


## Configuration du terminal de communication

Maintenant que MicroPython est présent sur le kit NUCLEO-WB55, nous voudrions tester la communication avec Python. Le test consiste à envoyer une commande Python et à vérifier que l'exécution se déroule correctement.

La communication s'effectue par USB par le biais d'un port série, nous avons donc besoin d'un logiciel capable d'envoyer les commandes Python sous forme de texte à la carte et de recevoir le résultat de l'exécution.

Ouvrir TeraTerm après l'avoir installé (si vous n'avez pas les droits admin sur votre PC, Puttytel est disponible en version portable dans la zone de téléchargement).

Sélectionnez l'option ***Serial*** et le ***port COM*** correspondant à votre carte (**COMx: USB Serial Device**) :<br>
![Image](site/assets/images/microPython/TeraTerm.PNG)<br>
Si le COM correspondant à la carte n'est pas disponible, vérifiez que le kit est bien énuméré sur le bon port COM, voir **Vérifier l'attribution du port COM**.

Configurez les champs suivants dans le menu ***Setup*** / ***Serial Port***<br>
![Image](site/assets/images/microPython/TeraTerm_Setup.png)


Une nouvelle fenêtre s'affiche :<br>
![Image](site/assets/images/microPython/TeraTermDisplay_1.png)


Si nécessaire, appuyez sur *CTRL+D* pour faire un **Reset software**.

Vous pouvez maintenant exécuter des commandes Python.<br>
```python
print("Hello World")
```

![Image](site/assets/images/microPython/TeraTermDisplay_2.png)

Nous avons maintenant terminé la programmation du firmware MicroPython.<br>
Nous pouvons exécuter des fonctions Python au travers du port COM.

Notre kit de développement NUCLEO-WB55 est désormais prêt à être utilisé avec Python.


Gardez la fenêtre TeraTerm ouverte, elle vous sera utile pour visualiser l'exécution des scripts.


## Ecrire et exécuter un script MicroPython

Nous allons voir dans cette partie comment éditer avec Notepad++ et exécuter un script Python sur notre carte NUCLEO-WB55.

Lancez Notepad++ après l'avoir installé (vous pouvez aussi utiliser la version portable). Cet outil vous permettra d'éditer vos scripts Python.

Ouvrez le fichier ***main.py*** présent sur la Nucleo (représenté par le lecteur PYBFLASH)<br>

Notre premier script va consister à afficher 10 fois le message `MicroPython est génial` avec le numéro du message à travers le port série du connecteur USB user de la carte NUCLEO.

Ecrivez l'algorithme suivant dans l'éditeur de script :

![Image](site/assets/images/microPython/Notepad_test1.png)

Prenez soin de sauvegarder le fichier (*CTRL+S*) ***main.py*** sur la carte (lecteur PYBFLASH)<br>.
Faites un software reset de la carte en appuyant sur *CTRL+D* dans votre terminal série (TeraTerm par exemple), le script est directement interprété!<br>

![Image](site/assets/images/microPython/TeraTerm_test1.png)

Le script s'est exécuté avec succès ; nous voyons bien notre message s'afficher 10 fois dans le terminal.<br>
Vous venez d'écrire votre premier script Python embarqué dans une carte NUCLEO-WB55!<br>

**Remarques importantes :**
Voici la [liste des libraires utilisables ou en cours de développement sous MicroPython](http://docs.micropython.org/en/latest/library/index.html#micropython-specific-libraries)<br>

## Annexe : Vérifier l'attribution du port COM

Un port `COM` a dû être créé sur Windows.

Ecrivez `gestionnaire de périphériques` dans la barre de recherche Windows, puis cliquez sur `Ouvrir` :

![image](site/assets/images/microPython/port_COM.png)

Une nouvelle fenêtre s'ouvre :

![image](site/assets/images/microPython/gestion_peripheriques.png)

Relevez le numéro du port ***COM***. Dans l'exemple ci-dessus, le kit NUCLEO-WB55 est branché sur le port ***COM3***. C'est ce numéro **COMx** qu'il faudra rentrer dans votre terminal série (TeraTerm...).
