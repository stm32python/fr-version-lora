# Introduction

This markdown helps centralizing informations used to modify the site's structure (not it's contents).

## The theme

For all reference to the jekyll's theme, you will find a lot of information in the TeXt Theme's site : [TeXt theme](https://tianqi.name/jekyll-TeXt-theme/).

It's GitHub page might help you too : [TeXt theme GitHub](https://github.com/kitian616/jekyll-TeXt-theme)

## Header

### Files and Directories
* _includes/header.html
* _data/navigation.yml
* _includes/svg/
* _config.yml
* fr/assets/flags/
* _sass/components/_header.scss

#### Logo's modification
They are in the `header_brand` div in the 10th line of the file `header.html`.

A logo can be added via `include` if it can be found in the `includes/svg`.

Or you can simply add it via `img`

#### Title's modification
The title can be found at the 21st line of the file `_config.yml`.

#### Page links modification
The list of accessible pages via the header is in the file `navigation.yml` in the `header` category line 1.

#### Gitlab and other language links modification.
The gitlab link is written in `header.html` in `nav class="navigation"` at line 42.

The list of other languages available with their logos and links are in the file `_config.yml` in the `language` category line 41.

## Footer navigation

#### Files and Directories
You can add a navigation system down the page before the footer with a button to give acces to the next/previous article.

The code generating it can be found at the line 37 of the file `article-section-navigation.html`.

This navigation is linked with the sidebar. And will require some work to make the two buttons lead you to other pages than the next one according to sidebar.
