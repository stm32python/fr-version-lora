# How to add/edit an tutorial or an index ?

## The Gitlab repository's structure
* The repository `site` contains all the french files and exercices (same for each languages).
* The file `_data/navigation.yml` contains the site summary's description (on the left).
* The file `_config.yml` contains the site's parameters used for it's generation.
* The directory `_layouts` contains the HTML pages's pattern.
* The directory `_includes` contains the HTML pages fragment's patterns used to generate the HTML pages.
* The HTML pages are generated in the `_site` directory.
* The directory `assets` contains the `main.scss` wich refers to the others scss.
* The directory `_sass` contains all the `.scss`.


## ADD TUTORIALS

Put the files `.md` in the repository corresponding to your tutorial.

Put the images used in your tutorial in the directory `assets` in the directory corresponding.

Add a cover to your tutorial in the directory `assets/cover`.

Update the layout order in the file `config.yml` with the name of the .md file you added.

## Update a tutorial

Make the change you need in the file `.md` you want.

If the name of the file has changed don't foerget to update the order in the file `config.yml`.

## Add a tutorial division

To add a new division on the site, add the division's index (Markdown file) in `_pages`.

Create a new directory in the directory `fr` (if it's a french tutorial). Don't forget the `_` before your directory's name.
Add your tutorials inside.

Update the file `_config.yml`. To do that, add a collection with your directory's name (without the `_`).

Finally, update the file `_data/navigation.yml` with your nex division.

## Edit locally

The files modifications can be done from the website https://gitlab.com/stm32python/fr-version-lora. However, ever modification engage the CI pipeline to regenerate the pages. The operation can take a while and consume energy.

Hopefully, you can edit locally and check the modifications easilly.

To do so, you need to have Jekyll installed in you computer [install Jekyll](https://jekyllrb.com/docs/installation/ubuntu/). 
Then write this in your terminal :
```
./run_local.sh
```
Open the page [local launch](http://127.0.0.1:4000/fr-version-lora/)